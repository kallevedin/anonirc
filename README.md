
AnonIRC!!<3
===========

IRC server built for usage with TOR/I2P/CJDNS and maybe other security networks.

This is abandonware. [Use MCXIRCD instead!](https://bitbucket.org/kallevedin/mcxircd)

The idea behind this is that everyone that connects to the IRC server will have their identities hidden, by the underlying network infrastructure (eg. TOR), and most IRC servers are NOT built for that scenario. For example, it is impossible to ban users based on their orginating addresses. It is also important that the IRC server does not allow users to circumvent the anonymizing measures taken by the users. Therefore, not all commands and features specified in the IRC RFCs should be implemented. Traditional IRC makes assumptions about the network infrastructure used by the users that does not hold true in anonymizing networks, such as TOR.
AnonIRC assumes that all users wants to stay hidden and as anonymous (or pseudonymous) as possible.

And, its fun when people are totally protected. The stuff people say when they KNOW they are secure is so fucked up and amazing!! :D 
AnonIRC is also a social experiment in the effects of anonymity. The weirdest and coolest social network on the intertubes! <3

Status
------
It works, but there is very much more to be done.

Features
--------
  * You can log into it and chat with others :-)
  * It collects minimal data about its users, and does not provide any unnecessary information about its users to other users.
  * It does not support encryption, as that is provided endpoint-to-endpoint by TOR/I2P/CJDNS. (Most client software and users appear not to enforce encryption or certificate validation, so my experience is that SSL encryption is pointless anyway.)
  * Silently filters CTCP commands, to remove all methods to query user clients about their user names, client software and their versions, their IP numbers, et.c., et.c.

Roadmap
-------
In maybe almost this order:

First release:

  * Message passing & Non-blocking I/O & thread pool
  * Anonymous channels
  * Cleaning up the code and making a nice release

Future releases:

  * ChanServ bot, to help users configure channels
  * Spam protection without need for hostnames (!)
  * Server configuration through an AnonIRCd bot/channel
  * Server-to-server protocol(s)
  * ngircd compatibility (AnonIRC<->ngircd server connect, so that its possible to easy to integrate with vanilla telecomix IRC)
  * STFU-command (channel- & server-wide ignore of user)

Bugs
----
Known bugs are

  * join messages displays wrong channel if mode +L redirect
  * MODE message for user instead of channel when user join channel

Author
------
Kalle Vedin, kalle.vedin@fripost.org
