package anonirc.util;

public class AUtil {

 	/**
	 * Returns the first word of a String
	 * @param string
	 * @return the first word
	 */
	public static String firstWord(String string) {
		string = string.trim();
		int spc = string.indexOf(' ');
		if(spc == -1) 
			return string;
		return string.substring(0, spc);
	}
	
}
