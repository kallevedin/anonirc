package anonirc;

import java.util.LinkedList;

import anonirc.messages.Message;

/**
 * Represents a perfectly normal channel. #channel, or HashChannel.
 * @author kaos
 *
 */
public class HashChannel implements IRCIdentity {

	private String name;
	
	public HashChannel(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(IRCIdentity o) {
		return this.name.compareTo(o.name());
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public void privmsg(IRCIdentity from, String message) {
		// TODO Auto-generated method stub
	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notice(IRCIdentity from, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getModeString(IRCIdentity caller) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

}
