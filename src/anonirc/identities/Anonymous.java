package anonirc.identities;

import java.util.LinkedList;

import anonirc.AnonIRC;
import anonirc.IRCIdentity;
import anonirc.messages.AIRC_OK;
import anonirc.messages.Message;

public class Anonymous implements IRCIdentity {

	private static Anonymous myself = new Anonymous();
	
	private Anonymous(){
		AnonIRC.addIRCIdentity(this);
	}
	
	public static Anonymous getSingleton() {
		return myself;
	}
	
	@Override
	public int compareTo(IRCIdentity o) {
		return this.name().compareTo(o.name());
	}
	
	@Override
	public String name() {
		return "Anonymous";
	}

	@Override
	public void privmsg(IRCIdentity from, String message) {
		// ignore
	}
	
	@Override
	public void notice(IRCIdentity from, String message) {
		// ignore
	}

	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {
		// ignore
	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
		// ignore
	}

	@Override
	public String getModeString(IRCIdentity caller) {
		return "";
	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments) {
		return new AIRC_OK(caller);
	}

	@Override
	public boolean isOperator() {
		return false;
	}

}
