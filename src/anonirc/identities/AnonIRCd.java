package anonirc.identities;

import java.util.LinkedList;
import anonirc.AnonIRC;
import anonirc.ConfigReader;
import anonirc.IRCIdentity;
import anonirc.messages.AIRC_OK;
import anonirc.messages.Message;
import anonirc.util.AUtil;

public class AnonIRCd implements IRCIdentity {

	private static AnonIRCd myself = new AnonIRCd();
	
	private AnonIRCd() {
		AnonIRC.addIRCIdentity(this);
	}
	
	public static AnonIRCd getSingleton() {
		return myself;
	}

	@Override
	public int compareTo(IRCIdentity o) {
		return this.name().compareTo(o.name());
	}
	
	@Override
	public String name() {
		return "AnonIRCd";
	}

	@Override
	public void privmsg(IRCIdentity from, String message) {
		if(from.isOperator()) {
			from.privmsg(this, "nothing implemented");
		} else {
			from.privmsg(this, "you need oper satus to do anything with me");
		}
	}

	@Override
	public void notice(IRCIdentity from, String message) {
	}

	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {	
	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
	}


	@Override
	public String getModeString(IRCIdentity caller) {
		return "";
	}

	@Override
	public boolean isOperator() {
		return true;
	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments) {
		return new AIRC_OK(caller);
	}

}
