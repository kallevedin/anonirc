package anonirc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Very simple logger.
 *
 */
public class Logger {

	private static Logger myself = new Logger(AnonIRC.logfile);  
	
	private String logfile;
	private BufferedWriter out;
	
	public static Logger getSingleton() {
		return Logger.myself;
	}
	
	private Logger(String logfile) {
		this.logfile = logfile;
		if(logfile == null)
			return;
	    try {
			out = new BufferedWriter(new FileWriter(logfile, true));
			out.write(System.currentTimeMillis() + " NEW AnonIRC log\n");
		} catch (IOException e) {
			System.err.println("ERROR. Logger could not open " + logfile);
			System.exit(128);
		}
	}
	
	public static boolean rehash() {
		myself.logfile = ConfigReader.lastConfigRead.logfile;
		try {
			myself.out = new BufferedWriter(new FileWriter(myself.logfile, true));
		} catch (IOException e) {
			System.err.println("Could not open new log file " + myself.logfile);
			return false;
		}
		return true;
	}
	
	public synchronized void log(String string) {
		if(logfile == null || logfile.equals("")) {
			System.out.println(string);
			return;
		} else {
			try {
				out.write(System.currentTimeMillis() + " " + string + "\n");
				out.flush();
			} catch (IOException e) {
				try {
					out = new BufferedWriter(new FileWriter(logfile, true));
					out.write(System.currentTimeMillis() + " reopened " + logfile + "\n");
					out.write(System.currentTimeMillis() + " " + string + "\n");
					out.flush();
				} catch (IOException e1) {
					System.err.println("ERROR. Logger could not open " + logfile);
				}
			}
		}
	}
	
}
