package anonirc;

import java.util.PriorityQueue;

/**
 * A background thread that sends PINGs to connected users (IRCClient's) that have not said anything in a while,
 * and kill()'s them if they do not reply in some time.
 * @author kaos
 *
 */
public class PingThread implements Runnable {

	private long allowedInactiveInterval = 3*60*1000; // 3 minutes
	private long pingTimeout = 2*60*1000; // 2 minutes
	
	/**
	 * Events are sorted according to when they should be checked
	 */
	private PriorityQueue<PingEvent> queue = new PriorityQueue<PingEvent>();

	/**
	 * A tuple of (timestamp, IRCClient), for sorting events in the queue
	 */
	private class PingEvent implements Comparable<PingEvent> {
		public long timestamp;
		public IRCClient client;
		public PingEvent(long timestamp, IRCClient client) {
			this.timestamp = timestamp;
			this.client = client;
		}
		@Override
		public int compareTo(PingEvent o) {
			long diff = this.timestamp - o.timestamp;
			if(diff > 0) return 1;
			else if(diff < 0) return -1;
			else return this.client.compareTo(o.client);
		}
	}
	
	/**
	 * Starts a new ping thread
	 */
	public PingThread() {
		(new Thread(this)).start();
	}
	
	/**
	 * Loops through the queue forever, keeping track of IRCClient's and their timeouts.
	 */
	public void run() {
		while (true) {
			try {
				long now = System.currentTimeMillis();
				PingEvent pe = queue.peek();
				if( pe == null ) {
					// there are no users connected
					Thread.sleep(this.allowedInactiveInterval);
				} 
				else if( pe.timestamp <= now ){
					queue.remove();
					if( AnonIRC.findIRCIdentity(pe.client.name()) == null ) {
						// the IRCClient has quit, so we just lets it drop out of the queue
					}
					else if( pe.client.waitingForPong ) {
						pe.client.kill("Ping timeout");
					}
					else if( pe.client.lastActive + allowedInactiveInterval < now ){
						pe.client.ping();
						this.queue.add(new PingEvent(now + pingTimeout, pe.client));
					} else {
						// user has been busy since last
						this.queue.add(new PingEvent(pe.client.lastActive + allowedInactiveInterval, pe.client));
					}
				} else {
					long timeToSleep = pe.timestamp - System.currentTimeMillis();
					Thread.sleep(timeToSleep);
				}
			} catch (InterruptedException e) {
				continue;
			}
		}
	}

	/**
	 * Adds an IRCClient to the gaze of the ping thread. It should only be called once for every IRCClient.
	 * @param client
	 */
	public void schedule(IRCClient client) {
		this.queue.add(new PingEvent(client.lastActive, client));
	}
	
}
