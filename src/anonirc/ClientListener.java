package anonirc;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientListener implements Runnable {

	private Thread thread;
	private int port;
	
	/**
	 * Listens to incoming connections and creates IRCClients
	 * @param port
	 */
	public ClientListener(int port){
		this.port = port;
		this.thread = new Thread(this);
		this.thread.run();
	}

	@SuppressWarnings("resource")
	@Override
	public void run() {
		ServerSocket serverSocket;
		
		try {
			serverSocket = new ServerSocket(this.port);
			while( true ){
				try {
					Socket clientSocket = serverSocket.accept();
					new IRCClient(clientSocket);
				} catch (IOException e) {
					// client socket failure, but the server socket is still there..
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// socket errors
			e.printStackTrace();
		}
		
	}
}
