package anonirc;

import java.util.LinkedList;

import anonirc.messages.Message;


/**
 * Anything that can be addressed, talked to, through or from, owns an IRCIdentity. Examples
 * are channels, users, bots, services and the pseudo-user 'Anonymous'.
 */
public interface IRCIdentity extends Comparable<IRCIdentity> { 
	
	/**
	 * A simple boolean does not cut it
	 */
	public enum modeUpdateStatus { OK, ERR_UNKNOWNMODE, ERR_USERSDONTMATCH, ERR_NOCHANMODES,
		ERR_USERNOTINCHANNEL, ERR_CHANOPRIVSNEEDED, AIRC_SECURITYVIOLATION, AIRC_TOMUCHINPUT, 
		ERR_ERR_NOSUCHNICK }
	
	/**
	 * Returns the name of the identity.
	 * @return
	 */
	public String name();
	
	/**
	 * Sends a direct PRIVMSG message to this identity.
	 * @param from
	 * @param message
	 */
	public void privmsg(IRCIdentity from, String message);
	
	/*
	 * TODO: Implement this everywhere
	 * MAYBE have a separate interface for this
	 * public void enqueMessage(Message);
	 */
	
	/**
	 * Sends a channel PRIVMSG message to this identity.
	 * @param channel
	 * @param from
	 * @param message
	 */
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message);
	
	/**
	 * Sends a direct NOTICE message to this identity.
	 * @param from
	 * @param message
	 */
	public void notice(IRCIdentity from, String message);
	
	/**
	 * Sends a channel NOTICE message to this identity.
	 * @param channel
	 * @param from
	 * @param message
	 */
	public void notice(IRCIdentity channel, IRCIdentity from, String message);

	/**
	 * Try to set the mode of an identity
	 * @param caller
	 * @param arguments a string like "+wi-o" and optional arguments 
	 * @return the status of the update
	 */
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments);
	
	/**
	 * Tries to return the mode string of an identity
	 * @param caller
	 * @return the string that should be shown to the user (or "?" if user is not allowed)
	 */
	public String getModeString(IRCIdentity caller);

	/**
	 * Returns true if the IRCIdentity is a network operator
	 * @return
	 */
	public boolean isOperator();
	
}
