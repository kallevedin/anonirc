package anonirc;

import java.util.LinkedList;
import java.util.TreeSet;

import anonirc.messages.Message;
import anonirc.messages.RPL_TOPIC;

public class BasicChannel implements IRCIdentity, ChannelInterface {

	private String name;
	private String topic;
	
	public BasicChannel(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(IRCIdentity o) {
		return this.name.compareTo(o.name());
	}

	@Override
	public String topic() {
		return topic;
	}

	@Override
	public Message getTopic(IRCClient client) {
		return null; //TODO new RPL_TOPIC(client, this);
	}

	@Override
	public Message join(IRCIdentity user, String key) {
		return null;
	}

	@Override
	public Message part(IRCIdentity user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeSet<IRCIdentity> cloneTreeOfIDsInChannel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message setTopic(IRCIdentity caller, String newTopic) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfUsers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isInChannel(IRCIdentity who) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isOp(IRCIdentity who) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isVoice(IRCIdentity who) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void kick(IRCIdentity caller, IRCIdentity whoToKick, String reason) {
		// TODO Auto-generated method stub

	}

	@Override
	public Message invite(IRCClient caller, String nickname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void privmsg(IRCIdentity from, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void notice(IRCIdentity from, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getModeString(IRCIdentity caller) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

}
