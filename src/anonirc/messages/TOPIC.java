package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * TOPIC message
 * @author kaos
 *
 */
public class TOPIC extends AbstractMessage {
	
	public IRCIdentity channel;
	public String newTopic;
	
	public TOPIC(IRCIdentity identityAffected, IRCIdentity channel, String newTopic) {
		super(identityAffected);
		this.channel = channel;
		this.newTopic = newTopic;
	}
	
	// there is only one version of this message ever displayed, see below
	@Override
	public String string() {
		return originString();
	}
	
	@Override
	public String originString() {
		return ":" + identityAffected.name() + "!" + identityAffected.name() + "@" + server.name() + " TOPIC " + channel.name() + " :" + newTopic;
	}

}
