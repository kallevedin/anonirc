package anonirc.messages;

import anonirc.IRCIdentity;

public class ERR_NOSUCHSERVER extends NumericErrorMessage {

	public String serverThatDoesNotExist;
	
	public ERR_NOSUCHSERVER(IRCIdentity identityAffected, String serverThatDoesNotExist) {
		super(identityAffected);
		this.serverThatDoesNotExist = serverThatDoesNotExist;
	}
	
	@Override
	public String string() {
		return "402 " + serverThatDoesNotExist + " :No such server";
	}

}
