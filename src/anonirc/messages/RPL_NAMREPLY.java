package anonirc.messages;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * RPL_NAMREPLY message, reply to NAMES (actually two messages)
 * @author kaos
 *
 */
public class RPL_NAMREPLY extends AbstractMessage {
	
	private Iterable<Channel> channels;
	private String namesReply;
	
	/**
	 * Generic NAMES reply for specific user
	 * @param identityAffected
	 */
	public RPL_NAMREPLY(IRCIdentity identityAffected) {
		super(identityAffected);
		channels = Channel.allChannelsOfUser(identityAffected);
		update();
	}
	
	/**
	 * NAMES reply for specific user and channel
	 * @param identityAffected
	 * @param channel
	 */
	public RPL_NAMREPLY(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		LinkedList<Channel> channels = new LinkedList<Channel>();
		channels.add(channel);
		this.channels = channels;
		update();
	}
	
	/**
	 * NAMES reply for specified list of channels
	 * @param identityAffected
	 * @param channels
	 */
	public RPL_NAMREPLY(IRCIdentity identityAffected, Iterable<Channel> channels) {
		super(identityAffected);
		this.channels = channels;
		update();
	}
	
	/**
	 * Updates the string returned by string().<br>
	 * This method is (usually?) only called in the constructor of RPL_NAMREPLY
	 */
	public synchronized void update() {
		int loops = 0;
		namesReply = serverPrefix() + "353 " + identityAffected.name() + " = ";
		for(Channel channel : channels) {
			loops++;
			namesReply += channel.name() + " :";
			Iterator<IRCIdentity> it = channel.cloneTreeOfIDsInChannel().iterator();
			while(it.hasNext()) {
				IRCIdentity user = it.next();
				if(channel.isOp(user)) namesReply += "@" + user.name();
				else if(channel.isVoice(user)) namesReply += "+" + user.name();
				else namesReply += user.name();
				if(it.hasNext()) namesReply += " "; // add space between users
			}
			namesReply += "\r\n";
		}
		// This is so ugly, but the last message depends on the previous ones...
		if(loops > 1)
			namesReply += serverPrefix() + "366 " + identityAffected.name() + " * :End of names list";
		else {
			Iterator<Channel> it = channels.iterator();
			if(it.hasNext()) {
				String chanName = it.next().name();
				namesReply += serverPrefix() + "366 " + identityAffected.name() + " " + chanName + " :End of names list";
			}
		}
	}
	
	@Override
	public synchronized String originString() {
		return namesReply;
	}
	
	@Override
	public synchronized String string() {
		return namesReply;
	}

}
