package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Resource temporarily unavailable-message
 * @author kaos
 *
 */
public class ERR_UNAVAILRESOURCE extends NumericErrorMessage {

	public IRCIdentity resource;
	
	public ERR_UNAVAILRESOURCE(IRCIdentity identityAffected, IRCIdentity resource) {
		super(identityAffected);
		this.resource = resource;
	}
	
	@Override
	public String string() {
		return "437 " + identityAffected.name() + " " + resource.name() + " :Nick/channel is temporarily unavailable"; 
	}

}
