package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Everything went OK-message, which should NOT be printed to human end-users
 * @author kaos
 *
 */
public class AIRC_OK extends AbstractMessage {

	public AIRC_OK(IRCIdentity identityAffected) {
		super(identityAffected);
		this.shouldBeWritten = false;
	} 

	@Override
	public String string() {
		return "999 OK";
	}

}
