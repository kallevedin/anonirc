package anonirc.messages;

import java.util.Iterator;
import java.util.LinkedList;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * "List of channels a user is inside"-message (should typically appear inside a RPL_WHOISUSER message-block)
 * @author kaos
 *
 */
public class RPL_WHOISCHANNELS extends AbstractMessage {

	public String nick;
	public Iterable<String> channelList;
	public String channelListString;
	public IRCIdentity whichUserIsThisAbout;
	
	/**
	 * Creates a RPL_WHOISCHANNEL from an iterable of strings
	 * @param identityAffected
	 * @param nick
	 * @param channelList
	 */
	public RPL_WHOISCHANNELS(IRCIdentity identityAffected, String nick, Iterable<String> channelList) {
		super(identityAffected);
		this.nick = nick;
		this.channelList = channelList;
	}
	
	/**
	 * Creates a RPL_WHOISCHANNELS from a string of channels, that NEEDS to be correctly formatted
	 * @param identityAffected
	 * @param nick
	 * @param channelListString
	 */
	public RPL_WHOISCHANNELS(IRCIdentity identityAffected, String nick, String channelListString) {
		super(identityAffected);
		this.nick = nick;
		this.channelListString = channelListString;
	}
	
	/**
	 * Automatically creates a RPL_WHOISCHANNELS message from *LOCAL* datastructures
	 * @param identityAffected
	 * @param whichUserIsThisAbout
	 */
	public RPL_WHOISCHANNELS(IRCIdentity identityAffected, IRCIdentity whichUserIsThisAbout) {
		super(identityAffected);
		channelListString = "";
		this.whichUserIsThisAbout = whichUserIsThisAbout; 
		Iterator<Channel> it = Channel.allChannelsOfUser(whichUserIsThisAbout).iterator();
		while(it.hasNext()) {
			Channel c = it.next();
			if( c.isOp(whichUserIsThisAbout) ) channelListString += "@" + c.name();
			else if( c.isVoice(whichUserIsThisAbout) ) channelListString += "+" + c.name();
			else channelListString += c.name();
			
			if(it.hasNext()) channelListString += " ";
		}
	}

	private Iterable<String> makeChannelList() {
		LinkedList<String> ll = new LinkedList<>();
		for(String s : channelListString.split(" +")){
			ll.addLast(s);
		}
		this.channelList = ll;
		return ll;
	}

	/**
	 * Returns a list of channels, each channel prefixed with either "@" or "+", depending on what
	 * status the user has in that channel
	 * @return
	 */
	public Iterable<String> getChannelListWithStatus() {
		if(channelList != null)
			return this.channelList;
		else
			return makeChannelList();
	}
	
	/**
	 * Returns a list of channels, without status prefixes
	 * @return
	 */
	public Iterable<String> getChannelListWithoutStatus() {
		LinkedList<String> ll = new LinkedList<>();
		for(String s : channelList) {
			ll.add(s.startsWith("@") || s.startsWith("+") ? s.substring(1) : s);
		}
		return ll;
	}
	
	/**
	 * Returns a simple string, as it would appear to a human user
	 * @return
	 */
	public String getChannelListString() {
		if(channelListString != null)
			return channelListString;
		else {
			String cls = "";
			Iterator<String> it = channelList.iterator();
			while(it.hasNext()) {
				cls += it.next() + (it.hasNext() ? " " : "");
			}
			return cls;
		}
	}
	
	@Override
	public String originString() {
		return serverPrefix() + string();
	}
	
	@Override
	public String string() {
		return "319 " + identityAffected.name() + " " + (whichUserIsThisAbout == null ? nick : whichUserIsThisAbout.name() ) + 
				" :" + getChannelListString();
	}

}
