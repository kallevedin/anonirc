package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * Message indicates that user can not join the channel because mode +l was set, and limit reached.
 * @author kaos
 *
 */
public class ERR_CHANNELISFULL extends NumericErrorMessage {

	public Channel channel;
	
	public ERR_CHANNELISFULL(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}
	
	@Override
	public String string() {
		return "471 " + identityAffected.name() + " " + channel.name() + " :Cannot join channel (+l)";
	}

}
