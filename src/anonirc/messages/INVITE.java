package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * INVITE message
 * @author kaos
 *
 */
public class INVITE extends AbstractMessage {

	public Channel channel;
	public IRCIdentity whoWasInvited;
	
	public INVITE(IRCIdentity identityAffected, Channel channel, IRCIdentity whoWasInvited) {
		super(identityAffected);
		this.channel = channel;
		this.whoWasInvited = whoWasInvited;
	}

	@Override
	public String originString() {
		return userPrefix() + "INVITE " + whoWasInvited.name() + " " + channel.name();
	}
	
	@Override
	public String string() {
		return originString();
	}

}
