package anonirc.messages;

import java.util.Iterator;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * "Channel mode is this-and-that"-message
 * @author kaos
 *
 */
public class RPL_CHANNELMODEIS extends AbstractMessage {

	public Channel channel;
	public String channelMode;
	public Iterable<String> modeParams;
	
	public RPL_CHANNELMODEIS(IRCIdentity identityAffected, Channel channel, String channelMode, Iterable<String> modeParams) {
		super(identityAffected);
		this.channelMode = channelMode;
		this.modeParams = modeParams;
	}

	@Override
	public String originString() {
		String modeParamStr = "";
		Iterator<String> it = modeParams.iterator();
		while(it.hasNext()) 
			modeParamStr += it.next() + (it.hasNext() ? " " : "");
		return serverPrefix() + "324 " + channel.name() + " " + channelMode + " " + modeParamStr;
	}
	
	@Override
	public String string() {
		return originString();
	}

}
