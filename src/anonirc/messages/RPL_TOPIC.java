package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * Message for notification of a channels TOPIC.
 * @author kaos
 *
 */
public class RPL_TOPIC extends AbstractMessage {

	public Channel channel;
	
	public RPL_TOPIC(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}

	@Override
	public String originString() {
		if(channel.topic() == null || channel.topic() == "") {
			return serverPrefix() + "331 " + identityAffected.name() + " " + channel.name() + " :No topic is set";
		} else {
			return serverPrefix() + "332 " + identityAffected.name() + " " + channel.name() + " :" + channel.topic();
		}
	}
	
	@Override
	public String string() {
		return originString();
	}

}
