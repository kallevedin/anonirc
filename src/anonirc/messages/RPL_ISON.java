package anonirc.messages;

import java.util.Iterator;
import java.util.LinkedList;

import anonirc.IRCIdentity;

/**
 * "These users are online"-message
 * @author kaos
 *
 */
public class RPL_ISON extends AbstractMessage {

	private String isOnString;
	private Iterable<String> isOnUsers;
	
	/**
	 * Creates an ISON message from an iterable
	 * @param identityAffected
	 * @param isOnUsers
	 */
	public RPL_ISON(IRCIdentity identityAffected, Iterable<String> isOnUsers) {
		super(identityAffected);
		this.isOnUsers = isOnUsers;
	}
	
	/**
	 * Creates an ISON message from a string (a string as viewed by a human user,
	 * if that user would be watching RAW IRC commands)
	 * @param identityAffected
	 * @param isOnString
	 */
	public RPL_ISON(IRCIdentity identityAffected, String isOnString) {
		super(identityAffected);
		this.isOnString = isOnString;
	}	
	
	/**
	 * Returns a human readable string (as it is represented in the RAW IRC protocol)
	 * @return
	 */
	public String isOnString() {
		if(isOnString != null)
			return isOnString;
		else {
			String s = "";
			Iterator<String> it = isOnUsers.iterator();
			while(it.hasNext()) {
				s += it.next() + (it.hasNext() ? " " : "");
			}
			this.isOnString = s;
			return s;
		}
	}
	
	/**
	 * returns an iterable of users that are online
	 * @return
	 */
	public Iterable<String> isOnUsers() {
		if(isOnUsers != null)
			return isOnUsers;
		else {
			LinkedList<String> ll = new LinkedList<>();
			for(String s : isOnString.split(" +")) {
				ll.add(s);
			}
			isOnUsers = ll;
			return ll;
		}
	}
	
	@Override
	public String originString() {
		return serverPrefix() + string();
	}
	
	@Override
	public String string() {
		return "303 " + identityAffected.name() + " :" + isOnString;
	}

}
