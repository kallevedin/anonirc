package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * User could not enter the channel because it was invite-only, and the user was not on the invite list :,-(
 * @author kaos
 *
 */
public class ERR_INVITEONLYCHAN extends NumericErrorMessage {

	public Channel channel;
	
	public ERR_INVITEONLYCHAN(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}
	
	@Override
	public String string() {
		return "473 " + identityAffected.name() + " " + channel.name() + " :Cannot join channel (+i)"; 
	}

}
