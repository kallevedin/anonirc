package anonirc.messages;

import anonirc.IRCIdentity;

public class ERR_NOSUCHNICK extends NumericErrorMessage {

	public String noSuchNick;
	
	public ERR_NOSUCHNICK(IRCIdentity identityAffected, String noSuchNick) {
		super(identityAffected);
		this.noSuchNick = noSuchNick;
	}

	@Override
	public String string() {
		return "401 " + identityAffected.name() + " " + noSuchNick + " :No such nick/channel";
	}

}
