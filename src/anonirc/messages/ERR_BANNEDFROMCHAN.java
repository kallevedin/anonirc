package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

public class ERR_BANNEDFROMCHAN extends NumericErrorMessage {

	public Channel channel;
	
	public ERR_BANNEDFROMCHAN(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}

	@Override
	public String string() {
		return "474 " + identityAffected.name() + " " + channel.name() + " :Cannot join channel (+b)";
	}

}
