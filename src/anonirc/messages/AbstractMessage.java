package anonirc.messages;

import anonirc.AnonIRC;
import anonirc.IRCIdentity;
import anonirc.identities.AnonIRCd;

/**
 * Abstract class that helps implementing IRC messages, by reducing code duplication
 * @author kaos
 *
 */
public abstract class AbstractMessage implements Message {

	protected IRCIdentity server;
	protected IRCIdentity identityAffected;
	protected boolean isError = false;
	protected boolean shouldBeWritten = true;
	
	protected AbstractMessage(IRCIdentity identityAffected) {
		this.identityAffected = identityAffected;
		this.server = AnonIRCd.getSingleton();
	}
	
	protected AbstractMessage(IRCIdentity server, IRCIdentity identityAffected) {
		this.identityAffected = identityAffected;
		this.server = server;
	}
	
	public boolean isError() {
		return this.isError;
	}
	
	public IRCIdentity getIdentityAffected() {
		return this.identityAffected;
	}
	
	public IRCIdentity getOriginatingServer() {
		return this.server;
	}
	
	/**
	 * Explicitly sets the originating server of this message
	 * @param server
	 */
	public void setServer(IRCIdentity server) {
		this.server = server;
	}
	
	@Override
	public IRCIdentity originator() {
		return server;
	}
	
	@Override
	public String originString() {
		return serverPrefix() + string();
	}
	
	@Override
	public boolean shouldBeWritten() {
		return this.shouldBeWritten;
	}
	
	/**
	 * Creates a user prefix for the message, when a message comes from a user at SOME (maybe even this) server
	 * @param id the user that the message came from
	 * @return
	 */
	protected String userPrefix(IRCIdentity id) {
		return ":" + id.name() + "!" + id.name() + "@" + server.name() + " ";
	}
	
	/**
	 * Creates a user prefix for the message, when a message comes from a user at SOME (maybe even this) server<br>
	 * Default: The message came from identityAffected
	 * @return
	 */
	protected String userPrefix() {
		return ":" + identityAffected.name() + "!" + identityAffected.name() + "@" + AnonIRC.hostcloak + " ";
	}
	
	/**
	 * Creates a server prefix for the message, when a message comes directly from a server
	 * @return
	 */
	protected String serverPrefix() {
		return ":" + AnonIRC.serverName + " ";
	}
	
}
