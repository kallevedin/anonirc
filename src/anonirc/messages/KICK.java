package anonirc.messages;

import anonirc.IRCIdentity;
import anonirc.identities.AnonIRCd;

/**
 * Kick message (does NOT have to mean that the target of the kick command should actually be kicked) 
 * @author kaos
 *
 */
public class KICK extends AbstractMessage {
	
	public IRCIdentity channel;
	public IRCIdentity whoWasKicked;
	
	public KICK(IRCIdentity identityAffected, IRCIdentity channel, IRCIdentity whoWasKicked) {
		super(identityAffected);
		this.channel = channel;
		this.whoWasKicked = whoWasKicked;
	}
	
	public KICK(IRCIdentity server, IRCIdentity identityAffected, IRCIdentity channel, IRCIdentity whoWasKicked) {
		super(server, identityAffected);
		this.channel = channel;
		this.whoWasKicked = whoWasKicked;
	}

	@Override
	public String string() {
		return "KICK " + channel + " " + whoWasKicked;
	}

}
