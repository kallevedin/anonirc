package anonirc.messages;

import anonirc.IRCIdentity;

public class RPL_LISTEND extends AbstractMessage {

	public RPL_LISTEND(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String string() {
		return "323 " + identityAffected.name() + " :End of LIST";
	}

}
