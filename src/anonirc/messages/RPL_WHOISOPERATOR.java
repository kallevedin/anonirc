package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * "This user is an IRC operator"-message (should only occur inside an RPL_WHOISUSER message-bleh
 * @author kaos
 *
 */
public class RPL_WHOISOPERATOR extends AbstractMessage {

	public String nick;
	
	public RPL_WHOISOPERATOR(IRCIdentity identityAffected, String nick) {
		super(identityAffected);
		this.nick = nick;
	}

	@Override
	public String originString() {
		return serverPrefix() + string();
	}
	
	@Override
	public String string() {
		return "313 " + identityAffected.name() + " " + nick + " :Is an IRC operator";
	}

}
