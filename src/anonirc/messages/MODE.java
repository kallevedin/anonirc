package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Mode message
 * @author kaos
 *
 */
public class MODE extends AbstractMessage {
	
	public IRCIdentity identityAffected;
	public String modeChange;
	
	/**
	 * MODE message
	 * @param targetOfMode who was affected by the mode
	 * @param modeChange new mode (the difference from original mode) of identity
	 */
	public MODE(IRCIdentity identityAffected, String modeChange) {
		super(identityAffected);
		this.modeChange = modeChange;
	}

	@Override
	public String originString() {
		return userPrefix() + string();
	}
	
	@Override
	public String string() {
		return identityAffected.name() + " " + modeChange;
	}
	
}
