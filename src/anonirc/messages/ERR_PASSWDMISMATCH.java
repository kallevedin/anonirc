package anonirc.messages;

import anonirc.IRCIdentity;

public class ERR_PASSWDMISMATCH extends NumericErrorMessage {

	public ERR_PASSWDMISMATCH(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String string() {
		return "464 " + identityAffected.name() + " :Password incorrect";
	}

}
