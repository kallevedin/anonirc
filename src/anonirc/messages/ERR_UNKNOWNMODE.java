package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * Error: Unknown mode
 * @author kaos
 *
 */
public class ERR_UNKNOWNMODE extends NumericErrorMessage {

	public String modeThatIsUnknown;
	public Channel channel;
	
	public ERR_UNKNOWNMODE(IRCIdentity identityAffected, Channel channel, String modeThatIsUnknown) {
		super(identityAffected);
		this.channel = channel;
		this.modeThatIsUnknown = modeThatIsUnknown;
	}

	@Override
	public String string() {
		return "472 " + identityAffected.name() + " " + modeThatIsUnknown + " :is unknown mode char to me for " + channel.name();
	}

}
