package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Generic error message
 * @author kaos
 *
 */
public class AIRC_ERR_GENERIC extends NumericErrorMessage {

	public String comment;
	public String referenceString;
	
	public AIRC_ERR_GENERIC(IRCIdentity identityAffected, String comment) {
		super(identityAffected);
		this.comment = comment;
	}

	@Override
	public String originString() {
		return serverPrefix() + "000 " + identityAffected.name() + " HERP DERP :" + comment;
	}
	
	@Override
	public String string() {
		return originString();
	}

}
