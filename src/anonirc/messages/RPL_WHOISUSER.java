package anonirc.messages;

import anonirc.AnonIRC;
import anonirc.IRCIdentity;

/**
 * "WHOIS-info begins"-message (should be followed by more RPL_*'s and ultimately ended by RPL_ENDOFWHOIS
 * @author kaos
 *
 */
public class RPL_WHOISUSER extends AbstractMessage {

	public String nick;
	public String user;
	public String host;
	
	/**
	 * Generates a generic RPL_WHOISUSER
	 * @param identityAffected
	 * @param nick
	 * @param user
	 * @param host
	 */
	public RPL_WHOISUSER(IRCIdentity identityAffected, String nick, String user, String host) {
		super(identityAffected);
		this.nick = nick;
		this.user = user;
		this.host = host;
	}
	
	/**
	 * Generates a RPL_WHOISUSER for this server (all cloaked and very censored)
	 * @param identityAffected
	 * @param nick
	 */
	public RPL_WHOISUSER(IRCIdentity identityAffected, String nick) {
		super(identityAffected);
		this.nick = nick;
		this.user = nick;
		this.host = AnonIRC.hostcloak;
	}	

	@Override
	public String originString() {
		// <nick> <user> <host> * :<real name>
		return serverPrefix() + string();
	}
	
	@Override
	public String string() {
		return "311 " + identityAffected.name() + " " + nick + " " + user + " " + host + " * :Chelsea Manning";
	}

}
