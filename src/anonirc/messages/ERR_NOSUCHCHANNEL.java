package anonirc.messages;

import anonirc.IRCIdentity;

public class ERR_NOSUCHCHANNEL extends NumericErrorMessage {

	public String thisChannelDoesNotExist;
	
	public ERR_NOSUCHCHANNEL(IRCIdentity identityAffected, String thisChannelDoesNotExist) {
		super(identityAffected);
		this.thisChannelDoesNotExist = thisChannelDoesNotExist;
	}

	@Override
	public String string() {
		return "403 " + identityAffected.name() + " " + thisChannelDoesNotExist + " :No such channel";
	}

}
