package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * "OPER status needed, and you ain't got any"-message
 * @author kaos
 *
 */
public class ERR_NOPRIVILEGES extends NumericErrorMessage {

	public ERR_NOPRIVILEGES(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String string() {
		return "481 " + identityAffected.name() + " :Permission denied- You're not an IRC operator";
	}

}
