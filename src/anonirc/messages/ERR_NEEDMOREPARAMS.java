package anonirc.messages;

import anonirc.AnonIRC;
import anonirc.IRCIdentity;

/**
 * "Need more parameters"-message
 * @author kaos
 *
 */
public class ERR_NEEDMOREPARAMS extends NumericErrorMessage {

	public ERR_NEEDMOREPARAMS(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String originString() {
		return ":" + AnonIRC.serverName + " 461 " + identityAffected + " INVITE :Not enough parameters";
	}
	
	@Override
	public String string() {
		return originString();
	}

}
