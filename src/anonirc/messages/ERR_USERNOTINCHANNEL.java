package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * ERROR: User is not in that channel
 * @author kaos
 *
 */
public class ERR_USERNOTINCHANNEL extends NumericErrorMessage {

	public String user;
	public Channel channel;
	
	public ERR_USERNOTINCHANNEL(IRCIdentity identityAffected, String user, Channel channel) {
		super(identityAffected);
		this.user = user;
		this.channel = channel;
	}

	@Override
	public String string() {
		return "441 " + identityAffected.name() + " " + user + " " + channel.name() + " :They aren't on that channel";
	}

}
