package anonirc.messages;

import anonirc.IRCIdentity;
import anonirc.identities.AnonIRCd;

public class PART extends AbstractMessage {

	IRCIdentity channel;
	
	public PART(IRCIdentity identityAffected, IRCIdentity channel) {
		super(AnonIRCd.getSingleton(), identityAffected);
		this.channel = channel;
	}
	
	public PART(IRCIdentity server, IRCIdentity identityAffected, IRCIdentity channel) {
		super(server, identityAffected);
		this.channel = channel;
	}

	@Override
	public boolean isError() {
		return false;
	}

	@Override
	public String string() {
		return identityAffected.name() + " PART " + channel.name();
	}

}
