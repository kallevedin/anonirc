package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

public class RPL_LIST extends AbstractMessage {

	public Channel channel;
	public boolean censor;
	
	public RPL_LIST(IRCIdentity identityAffected, Channel channel, boolean censor) {
		super(identityAffected);
		this.channel = channel;
		this.censor = censor;
	}

	@Override
	public String string() {
		if(censor)
			return "322 " + identityAffected.name() + " " + channel.name() + " 0 :N/A";
		else
			return "322 " + identityAffected.name() + " " + channel.name() + " " + channel.getNumberOfUsers() + " :" + channel.getTopic();
	}

}
