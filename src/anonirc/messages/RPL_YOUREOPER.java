package anonirc.messages;

import anonirc.IRCIdentity;

public class RPL_YOUREOPER extends AbstractMessage {

	public RPL_YOUREOPER(IRCIdentity identityAffected) {
		super(identityAffected);
	}
	
	@Override
	public String string() {
		return "381 " + identityAffected.name() + " :You are now an IRC operator";
	}

}
