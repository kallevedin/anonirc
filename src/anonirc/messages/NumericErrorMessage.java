package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Extended by all numeric ERROR messages
 * @author kaos
 *
 */
public abstract class NumericErrorMessage extends AbstractMessage {

	public NumericErrorMessage(IRCIdentity identityAffected) {
		super(identityAffected);
		this.isError = true;
	}
	
	@Override
	public String originString() {
		return serverPrefix() + string();
	}

}
