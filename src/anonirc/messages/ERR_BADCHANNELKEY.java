package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

public class ERR_BADCHANNELKEY extends NumericErrorMessage {

	public Channel channel;

	public ERR_BADCHANNELKEY(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}
	
	@Override
	public String string() {
		return "475 " + identityAffected.name() + " " + channel.name() + " :Cannot join channel (+k)";
	}

}
