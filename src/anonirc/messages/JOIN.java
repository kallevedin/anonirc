package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * JOIN message
 * @author kaos
 *
 */
public class JOIN extends AbstractMessage {

	public IRCIdentity channel;

	/**
	 * JOIN message
	 * @param identityAffected who joins?
	 * @param channel what is being joined by identityAffected
	 */
	public JOIN(IRCIdentity identityAffected, IRCIdentity channel) {
		super(identityAffected);
		this.channel = channel;
	}
	
	public JOIN(IRCIdentity originator, IRCIdentity identityAffected, IRCIdentity channel) {
		super(originator, identityAffected);
		this.channel = channel;
	}

	@Override
	public String originString() {
		return userPrefix() + "JOIN " + channel.name();
	}
	
	@Override
	public String string() {
		return originString();
	}

}
