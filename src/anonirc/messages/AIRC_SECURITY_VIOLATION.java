package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Sent because of generic security violations
 * @author kaos
 *
 */
public class AIRC_SECURITY_VIOLATION extends NumericErrorMessage {

	/** Comment describing the security violation */
	public String comment = "";
	/** Reference to what caused the security violation (channel, user, server, ???) */
	public IRCIdentity reference = null;
	
	public AIRC_SECURITY_VIOLATION(IRCIdentity identityAffected, String comment, IRCIdentity reference) {
		super(identityAffected);
		this.comment = comment;
		this.reference = reference;
	}

	@Override
	public String string() {
		return "999 " + identityAffected.name() + " " + reference.name() + " SECURITY VIOLATION :" + comment;
	}

}
