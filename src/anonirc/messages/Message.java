package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * Implemented by all IRC messages 
 *
 */
public interface Message {
	
	/**
	 * The origin of the message
	 * @return
	 */
	public IRCIdentity originator();
	
	/**
	 * True, if this is a an error
	 * @return
	 */
	public boolean isError();
	
	/**
	 * The message represented as a string, without originator prefix
	 * @return
	 */
	public String string();
	
	/**
	 * The message represented as a string, with originator prefix
	 * @return
	 */
	public String originString();

	/**
	 * Returns true if the message should be written to a human user, a connected server, or similar.<br>
	 * Silent messages is AIRC_OK, which is just a "silent success".
	 * @return
	 */
	public boolean shouldBeWritten();
	
}
