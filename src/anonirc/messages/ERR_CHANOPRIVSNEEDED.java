package anonirc.messages;

import anonirc.Channel;
import anonirc.IRCIdentity;

/**
 * The user does not have the sufficient privileges to do whatever it did
 * @author kaos
 *
 */
public class ERR_CHANOPRIVSNEEDED extends NumericErrorMessage {

	public Channel channel;
	
	public ERR_CHANOPRIVSNEEDED(IRCIdentity identityAffected, Channel channel) {
		super(identityAffected);
		this.channel = channel;
	}

	@Override
	public String string() {
		return serverPrefix() + "482 " + identityAffected.name() + " " + channel.name() + " :You're not channel operator";
	}

}
