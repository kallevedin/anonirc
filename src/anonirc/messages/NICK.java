package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * NICK message
 * @author kaos
 *
 */
public class NICK extends AbstractMessage {
	
	String oldNick;
	String newNick;
	
	public NICK(IRCIdentity identityAffected, String oldNick, String newNick) {
		super(identityAffected);
		this.oldNick = oldNick;
		this.newNick = newNick;
	}

	public String oldNick() {
		return this.oldNick;
	}
	
	public String newNick() {
		return this.newNick;
	}
	
	@Override
	public boolean isError() {
		return false;
	}

	@Override
	public String string() {
		return "NICK " + identityAffected.name();
	}

	@Override
	public String originString() {
		return userPrefix(identityAffected) + " NICK " + newNick;
	}

	
	
}
