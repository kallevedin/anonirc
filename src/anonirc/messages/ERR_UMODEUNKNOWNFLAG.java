package anonirc.messages;

import anonirc.IRCIdentity;

public class ERR_UMODEUNKNOWNFLAG extends NumericErrorMessage {

	public ERR_UMODEUNKNOWNFLAG(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String string() {
		return "501 " + identityAffected.name() + " :Unknown MODE flag";
	}

}
