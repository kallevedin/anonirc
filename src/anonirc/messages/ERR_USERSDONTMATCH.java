package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * "Can not change mode for other users"-message
 * @author kaos
 *
 */
public class ERR_USERSDONTMATCH extends NumericErrorMessage {

	public ERR_USERSDONTMATCH(IRCIdentity identityAffected) {
		super(identityAffected);
	}

	@Override
	public String string() {
		return "502 " + identityAffected.name() + " :Cannot change mode for other users";
	}

}
