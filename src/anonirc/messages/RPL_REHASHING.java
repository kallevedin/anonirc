package anonirc.messages;

import anonirc.IRCIdentity;

public class RPL_REHASHING extends AbstractMessage {

	public String configFile;
	
	public RPL_REHASHING(IRCIdentity identityAffected, String configFile) {
		super(identityAffected);
		this.configFile = configFile;
	}

	@Override
	public String string() {
		return "382 " + identityAffected.name() + " " + configFile + " :Rehashing";
	}

}
