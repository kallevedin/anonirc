package anonirc.messages;

import anonirc.IRCIdentity;

/**
 * "The mode of a user is..."-message
 * @author kaos
 *
 */
public class RPL_UMODEIS extends AbstractMessage {

	public String modeString;
	
	public RPL_UMODEIS(IRCIdentity identityAffected, String modeString) {
		super(identityAffected);
		this.modeString = modeString;
	}

	@Override
	public String string() {
		return "221 " + identityAffected.name() + " " + modeString;
	}

}
