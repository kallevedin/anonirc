package anonirc.messages;

import anonirc.AnonIRC;
import anonirc.IRCIdentity;

public class RPL_ENDOFWHOIS extends AbstractMessage {

	public String nick;
	
	public RPL_ENDOFWHOIS(IRCIdentity identityAffected, String nick) {
		super(identityAffected);
		this.nick = nick;
	}

	@Override
	public String originString() {
		return serverPrefix() + string();
	}
	
	@Override
	public String string() {
		return "318 " + identityAffected.name() + " " + nick + " :End of WHOIS list";
	}

}
