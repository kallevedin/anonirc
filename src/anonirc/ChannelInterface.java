package anonirc;

import java.util.TreeSet;
import anonirc.messages.Message;

public interface ChannelInterface extends IRCIdentity {

	/**
	 * Returns the topic
	 * @return
	 */
	public String topic();

	/**
	 * Returns topic-message to the caller
	 * @param client
	 * @return
	 */
	public Message getTopic(IRCClient client);
	
	/**
	 * Joins the channel, with the specified key (which is like a password)
	 * @return
	 */
	public Message join(IRCIdentity user, String key);

	/**
	 * Departs from the channel
	 * @param user
	 * @return
	 */
	public Message part(IRCIdentity user);

	/**
	 * Copy of the user list, in the form of a TreeSet 
	 * @return
	 */
	public TreeSet<IRCIdentity> cloneTreeOfIDsInChannel();
	
	/**
	 * Sets the topic of the channel
	 * @param caller
	 * @param newTopic
	 */
	public Message setTopic(IRCIdentity caller, String newTopic);
	
	/**
	 * Returns the number of users in the channel
	 * @return
	 */
	public int getNumberOfUsers();

	/**
	 * Returns true if the specified user is in the channel
	 * @param who
	 * @return
	 */
	public boolean isInChannel(IRCIdentity who);
	
	/**
	 * Returns true if the specified user is channel operator
	 * @param who
	 * @return
	 */
	public boolean isOp(IRCIdentity who);
	
	/**
	 * Returns true if the specified user have voice.
	 * @param who
	 * @return
	 */
	public boolean isVoice(IRCIdentity who);
	
	/**
	 * Forcefully evicts a user from a channel
	 * @param caller
	 * @param whoToKick
	 * @param reason
	 */
	public void kick(IRCIdentity caller, IRCIdentity whoToKick, String reason);
	
	/**
	 * Invites a user to this channel
	 * @param caller who is inviting
	 * @param nickname the name of whoever is being invited
	 */
	public Message invite(IRCClient caller, String nickname);
	
	
}
