package anonirc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.TreeSet;

import anonirc.messages.*;
import anonirc.util.AUtil;



/**
 * IRCClient represents a user that is connected to the IRC server.
 * @author kaos
 *
 */
public class IRCClient implements Runnable, IRCIdentity {
	
	/* Secion 2.3.1 in RFC2812: nickname   =  ( letter / special ) *8( letter / digit / special / "-" ) */
	public final static String NICKNAME_REGEX = "[a-zA-Z\\[\\]\\\\`_\\^\\{\\}]{1}[a-zA-Z0-9\\-\\[\\]\\\\`_\\^\\{\\}]*";
	
	private static PingThread pingProcess = new PingThread();
	private static Logger logger = Logger.getSingleton();
	
	/** this is the IRCClients *personal* sessionID number */
	private long sessionID = AnonIRC.getUniqueID();

	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private Random random = new Random();
	
	private boolean alive = false;
	private String name = null;
	
	// user modes
	private boolean invisible = false;
	private boolean wallops = false;
	private boolean operator = false;
	
	// used by PingThread
	public long lastActive = System.currentTimeMillis();
	public boolean waitingForPong = false;
	
	/**
	 * Creates a new IRCClient that interprets messages from the specified socket, using a separate thread just for that.
	 * @param socket
	 */
	public IRCClient(Socket socket) {
		this.socket = socket;
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			alive = false;
			return;
		}
		alive = true;
		(new Thread(this)).start();
	}
	
	@Override
	public void run() {
		// initialization phase: handshake and registering nickname
		logger.log("SID " + this.sessionID + " created");
		name = handshake();
		if( name == null ) {
			try {
				socket.close();
			} catch (IOException e) {
			}
			return;
		}
		if(!AnonIRC.addIRCIdentity(this))
			return;
		motd();
		pingProcess.schedule(this);
		logger.log("handshake: " + this.name + "->SID" + this.sessionID);
		
		// alive phase: reading input and acting on that
		while( alive ){
			try {
				String str = in.readLine();
				if(str == null) 
					break;
				this.lastActive = System.currentTimeMillis();
				interpretCommand( str.trim() );
			} catch (IOException e) {
				if(alive) 
					e.printStackTrace();
				break;
			}catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
		
		// destroy phase: removing client and killing thread.
		Channel.forgetEverythingAboutThisClient(this);
		AnonIRC.removeIRCIdentity(this);
		logger.log("destroyed: " + this.name + "@" + this.sessionID);
	}

	/**
	 * Returns true if the nickname is allowed (RFC1459)
	 * @param name
	 * @return
	 */
	private boolean nicknameValid(String name) {
		if( name.matches(NICKNAME_REGEX) )
			return true;
		return false;
	}
	
	/**
	 * Returns true if the nickname already is taken
	 * @param name
	 * @return
	 */
	private boolean nicknameTaken(String name) {
		return AnonIRC.findIRCIdentity(name) != null ? true : false;
	}
	
	/**
	 * Performs an IRC-handshake at the TCP connection, accepting USER, PASS and 
	 * NICK commands, but pretty much ignoring everything except NICK.
	 * @return the nickname of the user
	 */
	private String handshake() {
		String name = null;
		
		while(name == null && alive) {
			String line;
			try {
				line = in.readLine();
			} catch (IOException e) {
				break;
			}
			if(line == null) 
				break;
			if(line.startsWith("CAP")) {
				// IRC Client Capabilities Extension - IGNORED
				continue;
			}
			else if(line.startsWith("SERVER")) {
				println("ERROR :SERVER command not supported");
				logger.log("error: @SID" + this.sessionID + " issued SERVER command");
				return null;
			}
			else if(line.startsWith("SERVICE")) {
				println("ERROR :SERVICE command not supported");
				logger.log("error: @SID" + this.sessionID + " issued SERVICE command");
				return null;
			}
			else if(line.startsWith("USER")) {
				// ignore everything except an optionally valid user mode mask (see RFC2812 3.1.3)
				String[] strs = line.split(" +|:.*");
				if(strs.length == 4) {
					if( strs[2].equals("4") )
						wallops = true;
					else if( strs[2].equals("8") )
						invisible = true;
					else if( strs[2].equals("12") )
						wallops = invisible = true;
				}
			}
			else if(line.startsWith("PASS"))
				continue;
			else if(line.startsWith("NICK")) {
				String[] str = line.split(" +");
				if(str.length == 2) {
					if(!nicknameValid(str[1])){
						println(":" + AnonIRC.serverName + " 432 " + str[1] + " :Erroneus nickname");
						continue;
					}
					if(nicknameTaken(str[1])){
						println(":" + AnonIRC.serverName + " 433 " + str[1] + " :Nickname is already in use");
						continue;
					}
					return str[1];
				}
				else break;
			} else break;
		}
		println("ERROR :Handshake failure.");
		logger.log("handshake failure: @SID" + this.sessionID);
		return null;
	}
	
	/*
	 * TODO: Implement message passing, as a queue of IRCClients that wait to be processed, such that the queue
	 * only contains ONE instance of each IRCClient, and such that each IRCClient has a "mailbox" of messages
	 * waiting to be processed. 
	 * Then add the process pool that iterates over the queue...
	 * 
	static private TreeSet<IRCClient> clientsWaiting = new TreeSet<>();
	static private LinkedList<IRCClient> clientQueue = new LinkedList<>();
	private LinkedList<Message> messageQueue = new LinkedList<>();
	public void enqueMessage(Message m) {
		messageQueue.addLast(m);
		if(clientsWaiting.contains(this)) {
			// ...
		} else {
			clientsWaiting.add(this);
		}
	}
	*/
	
	/**
	 * Prints the servers message of the day to the client.
	 */
	private void motd() {
		println(":" + AnonIRC.serverName + " 375 " + name + " :MOTD");
		for(String ml : AnonIRC.motd.motdLines) {
			println(":" + AnonIRC.serverName + " 375 " + name + " " + ml);
		}
		println(":" + AnonIRC.serverName + " 376 " + name + " :End of MOTD command");
	}
	
	/**
	 * Returns the nickname of the user
	 */
	public String name() {
		return this.name;
	}
	
	/**
	 * Sends the raw message-string to the IRC Client, with a \r\n at the end (because IRC newlines is like that)
	 * @param string
	 */
	public void println(String string){
		this.out.print(string + "\r\n");
		this.out.flush();
	}
	
	/**
	 * Returns true if the message contains a CTCP command other than ACTION (/me).
	 * @param message
	 * @return
	 */
	private boolean containsUnallowedCTCP(String message){
		byte[] inBytes = message.getBytes();
		if(inBytes.length > 0 && inBytes[0] == 0x01 )
			if( !AUtil.firstWord(new String(inBytes,1,inBytes.length-1)).equals("ACTION") )
				return true;
		return false;
	}
	
	/**
	 * Acts on the raw input of a connected IRC user
	 * @param input the raw string
	 */
	private void interpretCommand(String input) {
		String[] strs;
		int separator;
		IRCIdentity chan;
		
		if(input == null || input.length() == 0)
			return;
		
		/*
		 * this HUMONGOUS switch block is where the IRC server will parse incoming messages from connected clients.
		 * In order to understand this, you probably need to read RFC2812 (https://tools.ietf.org/html/rfc2812)
		 */
		switch(AUtil.firstWord(input).toUpperCase()) {
		case "" :
			break;
		
		/*
		 * PONG are replies sent to PING messages, to keep the connection alive.
		 * We ignore everything but the fact that a PONG was sent by the user.
		 */
		case "PONG" :
			this.waitingForPong = false;
			break;
		
		/*
		 * PINGs are sent by the user to 1) keep the connection alive, 2) learn about lag (in which case it also
		 * delivers a timestamp), 3) to ping others via the server.
		 */
		case "PING" :
			strs = input.split(" +");
			if(strs.length == 2){
				println("PONG " + strs[1]);	
			} else {
				println("PONG");
			}
			break;
			
		/*
		 * Client tells us that it disconnects. IRC protocol demands that we send an ERROR message back to it (!).
		 */
		case "QUIT" :
			logger.log("user " + this.name + "@SID" + this.sessionID + " issued QUIT command");
			this.kill("bye");
			break;
		
		/*
		 * Client wants to change nickname.
		 */
		case "NICK" :
			strs = input.split(" +");
			if(strs.length != 2) {
				syntaxError(input);
				return;
			} else if(!this.nicknameValid(strs[1])) {
				println(":" + AnonIRC.serverName + " 432 " + strs[1] + " :Erroneus nickname");
				return;
			} else if(AnonIRC.findIRCIdentity(strs[1]) != null) {
				println(":" + AnonIRC.serverName + " 433 " + strs[1] + " :Nickname is already in use");
				return;
			} else {
				String oldName = this.name;
				this.name = strs[1];
				Channel.updateNicknameInAllChannels(this, oldName, strs[1]);
				AnonIRC.updateNameOfIdentity(oldName, this);
				logger.log("NICK update: " + this.name + "@" + this.sessionID);
			}
			break;
		
		/*
		 * ISON is used by client software, such as pidgin, to be able to show if friends are connected to the
		 * network, despite maybe not being in any channels. ISON = "is on(line)?"
		 */
		case "ISON" :
			strs = input.split(" +");
			LinkedList<String> usersOnline = new LinkedList<>();
			for(int i=1; i!=strs.length; i++) {
				if(AnonIRC.findIRCIdentity(strs[i]) != null ) 
					usersOnline.add(strs[i]);
			}
			println(new RPL_ISON(this, usersOnline).originString());
			break;
		
			
		/* Joins a channel */	
		case "JOIN" :
			strs = input.split(" +");
			String key = "";
			if(strs.length == 2 && strs[1].equals("0")) {
				for(Channel c : Channel.allChannelsOfUser(this)) {
					c.part(this);
				}
				break;
			}
			if(strs.length == 3)
				key = strs[2];
			if(strs.length == 2 || strs.length == 3) {
				for(String channelName : strs[1].split(",")) {
					chan = AnonIRC.findIRCIdentity(channelName);
					if( chan == null ) {
						// create and join channel
						if( Channel.validChannelName(channelName) ) {
							Channel channel = Channel.create(channelName);	
							Message m = channel.join(this, key);
							if(m.shouldBeWritten())
								println(m.originString());
							if(!m.isError()) {
								println(channel.getTopic(this).originString());
								println(new RPL_NAMREPLY(this, channel).originString());
							}
						} else {
							println(new ERR_NOSUCHCHANNEL(this, channelName).originString());
						}
					} else {
						// join already existing channel
						Message m = ((Channel)chan).join(this, key);
						if(m.isError()) {
							println(m.originString());
						} else {
							if(m.shouldBeWritten()) {
								println(m.originString());
							}
							if(!m.isError()) {
								println(((Channel) chan).getTopic(this).originString());
								println(new RPL_NAMREPLY(this, (Channel)chan).originString());
							}
						}
					}
				}
			} else {
				syntaxError(input);
			}
			break;
			
		
		/*
		 * Departs from a channel
		 */
		case "PART" :
			strs = input.split(" +");
			if(strs.length == 2) {
				chan = AnonIRC.findIRCIdentity(strs[1]);
				if( chan instanceof Channel ) {
					Message m = ((Channel)chan).part(this);
					if(m.shouldBeWritten())
						println(m.string());
				} else {
					println(new ERR_NOSUCHNICK(this, strs[1]).originString());
				}
			} else {
				syntaxError(input);
			}
			break; 

		/*
		 * Sets the topic of a channel
		 */
		case "TOPIC" :
			separator = input.indexOf(':');
			if( separator == -1 ) {
				syntaxError(input);
				return;
			}
			String chanName = input.substring("TOPIC".length(), separator).trim();
			chan = AnonIRC.findIRCIdentity(chanName);
			if(chan instanceof Channel) {
				// TODO check if user is allowed to change topic
				String newTopic = input.substring(separator + 1);
				Message m = ((Channel)chan).setTopic(this, newTopic);
				if(m.shouldBeWritten()) 
					println(m.originString());
			} else {
				println(new ERR_NOSUCHCHANNEL(this, chanName).originString());
			}
			break;
		
		/*
		 * Sends a message to either a channel or a specific user
		 */
		case "PRIVMSG" :
			strs = input.split(" +|:.*");
			if(strs.length == 2) {
				separator = input.indexOf(':');
				if(separator == -1) {
					syntaxError(input);
					break;
				}
				String message = input.substring(separator+1);
				if( containsUnallowedCTCP(message) )
					break;
				
				IRCIdentity receiver = AnonIRC.findIRCIdentity(strs[1]);
				if( receiver != null )
					receiver.privmsg(this, message);
				else 
					println(new ERR_NOSUCHNICK(this, strs[1]).originString());
			}
			break;

		// exactly the same as PRIVMSG, but according to RFC2812, it shall not return any error messages
		case "NOTICE" :
			strs = input.split(" +|:.*");
			if(strs.length == 2) {
				separator = input.indexOf(':');
				if(separator == -1) 
					break;
				String message = input.substring(separator+1);
				if(containsUnallowedCTCP(message))
					break;
				IRCIdentity receiver = AnonIRC.findIRCIdentity(strs[1]);
				if(receiver != null)
					receiver.notice(this, message);	
			}
			break;
		
			
		/*
		 * List all users that are directly visible to the user
		 * Syntax: NAMES [ <channel> *( "," <channel> ) [ <target> ] ]
		 * Three variants:
		 * 		0 arguments: List *all* users that are visible to the user issuing this command
		 * 		1 argument: List all users that are visible in a list of channels (1st argument)
		 * 		2 arguments: Try to forward the message to <target> -- NOT ALLOWED
		 */
		case "NAMES" :
			strs = input.split(" +");
			String[] channels;
			if(strs.length == 3) {
				// User tried to forward NAMES command to other server -- NOT ALLOWED 
				println(new ERR_NOSUCHSERVER(this, strs[2]).originString());
				break;
			} else if(strs.length == 2) {
				// User supplied a comma-separated list of channels as 2nd argument
				channels = strs[1].split(",");
				for(int i=0; i!=channels.length; i++) {
					IRCIdentity id = AnonIRC.findIRCIdentity(channels[i]);
					if(id instanceof Channel) {
						Channel c = (Channel)id;
						if(c.isInChannel(this)) {
							println(new RPL_NAMREPLY(this, c).originString());
						}
					}
				}
			} else if(strs.length == 1) {
				// User wish to list all users visible to her
				TreeSet<Channel> chnls = Channel.allChannelsOfUser(this);
				for(Channel c : chnls) {
					println(new RPL_NAMREPLY(this, c).originString());
				}
			} else {
				this.syntaxError(input);
				break;
			}
			break;
			
			
		/*
		 * Forcibly departs one or more users from a channel
		 * Command syntax:
		 * 		KICK #channel username(,username)* (:reason){0,1}
		 * 
		 */
		case "KICK" :
			strs = input.split(" +|:.*");
			String reason = "";
			
			if(strs.length == 3){
				chan = AnonIRC.findIRCIdentity(strs[1]);
				if( chan instanceof Channel) {
					Channel channel = ((Channel)chan);
					if( channel.isOp(this) ){
						String[] listOfBadUsers = strs[2].split(",");
						LinkedList<IRCIdentity> usersToKick = new LinkedList<IRCIdentity>();
						for(String name : listOfBadUsers) {
							IRCIdentity userId = AnonIRC.findIRCIdentity(name);
							if(userId != null) {
								if(channel.isInChannel(userId)) {
									usersToKick.add(userId);
								} else {
									println(new ERR_USERNOTINCHANNEL(this, name, channel).originString());
									return;
								}
							} else {
								println(new ERR_NOSUCHNICK(this, name).originString());
								return;
							}
						}
						separator = input.indexOf(':');
						if(separator != -1) {
							reason = input.substring(separator+1);
						}
						for(IRCIdentity badUser : usersToKick) {
							channel.kick(this, badUser, reason);
						}
					} else {
						println(new ERR_CHANOPRIVSNEEDED(this, channel).originString());
					}
				} else {
					println(new ERR_NOSUCHCHANNEL(this, strs[1]).originString());
				}
			} else {
				syntaxError(input);
			}
			break;

		/*
		 * Sets the mode of a channel or user
		 */
		case "MODE" :
			strs = input.split(" +");
			if( strs.length == 2 ) {
				// show mode
				IRCIdentity id = AnonIRC.findIRCIdentity(strs[1]);
				if(id != null) {
					String modeString = id.getModeString(this);
					if(modeString == null) {
						println(new ERR_NOPRIVILEGES(this).originString());
						modeString = "?";
					}
					println(new RPL_UMODEIS(this, modeString).originString());
					break;
				} else {
					println(new ERR_NOSUCHNICK(this, strs[1]).originString());
					break;
				}
			}
			if( strs.length >= 3 ) {
				// set mode
				IRCIdentity id = AnonIRC.findIRCIdentity(strs[1]);
				if( id == null ) {
					println(new ERR_NOSUCHNICK(this, strs[1]).originString());
					return;
				}
				// create 2nd argument to id.setMode()
				LinkedList<String> modeStrs = new LinkedList<>();
				for(int i=2; i!=strs.length; i++)
					modeStrs.addLast(strs[i]);
				// perform update and print result
				Message m = id.setMode(this, modeStrs);
				if(m.shouldBeWritten())
					println( m.originString() );
			}
			else {
				syntaxError(input);
			}
			break;
		
			
		/*
		 * Query the server to drop info about specific user. Parse command and return minimal info.
		 */
		case "WHOIS" :
			strs = input.split(" +");
			String nicks;
			if(strs.length == 3){
				// server target parameter was given
				if(!strs[1].equals(AnonIRC.serverName)){
					println(new ERR_NOSUCHSERVER(this, strs[1]).originString());
					break;
				}
				nicks = strs[2];
			} else if ( strs.length == 2) {
				nicks = strs[1];
			} else {
				syntaxError(input);
				break;
			}
			for(String nick : nicks.split(",")) {
				IRCIdentity id = AnonIRC.findIRCIdentity(nick);
				if(id != null) {
					println(new RPL_WHOISUSER(this, nick).originString());
					if(id.isOperator())
						println(new RPL_WHOISOPERATOR(this, nick).originString());
					if( this.isOperator() ) // I consider what channels a user is on, to be private information
						println(new RPL_WHOISCHANNELS(this, id).originString());
					println(new RPL_ENDOFWHOIS(this, nick).originString());
				} else {
					println(new ERR_NOSUCHNICK(this, nick).originString());
				}
			}
			break;
		
		/*
		 * Users are not allowed to update their usernames after they have performed their handshakes.
		 * This is not very important, as the user command is mostly ignored anyways.
		 */
		case "USER" :
			println(":" + AnonIRC.serverName + " 462 " + this.name + " :Connection already registered");
			break;

		case "INVITE" :
			strs = input.split(" +");
			if(strs.length != 3) {
				println(new ERR_NEEDMOREPARAMS(this).originString());
				break;
			}
			IRCIdentity id = AnonIRC.findIRCIdentity(strs[2]);
			if(id instanceof Channel) {
				Message m = ((Channel)id).invite(this, strs[1]);
				if(m.shouldBeWritten())  
					println(m.originString());
			} else {
				println(new ERR_NOSUCHCHANNEL(this, strs[2]).originString());
			}
			break;

		case "LIST" :
			strs = input.split(" +");
			if(strs.length == 3) {
				// will not forward this message to any other server
				println(new ERR_NOSUCHSERVER(this, strs[2]).originString());
			} else if(strs.length == 2) {
				// 1st argument is comma-separated list of channels
				// show only the existence of the channel unless the user is in the channel / is oper
				for(String channel : strs[1].split(",")) {
					IRCIdentity chanId = AnonIRC.findIRCIdentity(channel);
					if(chanId instanceof Channel) {
						boolean censor = !((Channel)chanId).isInChannel(this) || this.isOperator();
						println(new RPL_LIST(this, (Channel)chanId, censor).originString());
					} else {
						// no error reply
					}
				}
				println(new RPL_LISTEND(this).originString());
			} else if(strs.length == 1 ){
				// no arguments: list all channels that exist
				for(Channel c : Channel.allChannels()) {
					boolean censor = !(c.isInChannel(this) || this.isOperator());
					println(new RPL_LIST(this, c, censor).originString());
				}
				println(new RPL_LISTEND(this).originString());
			} else {
				syntaxError(input);
			}
			break;
			
		case "KILL" :
			if(this.isOperator()) {
				String killReason = "KILLed";
				strs = input.split(" +");
				if(strs.length >= 2) {
					IRCIdentity badUser = AnonIRC.findIRCIdentity(strs[1]);
					if(badUser != null) {
						if(badUser instanceof IRCClient) {
							((IRCClient)badUser).kill(killReason);
						} else {
							// can only KILL normal users now
						}
					} else {
						println(new ERR_NOSUCHNICK(this, strs[1]).originString());
					}
				} else {
					syntaxError(input);
				}
			} else {
				println(new ERR_NOPRIVILEGES(this).originString());
			}
			break;

		case "OPER" :
			strs = input.split(" +");
			if(strs.length == 3) {
				if( ConfigReader.lastConfigRead.operLogin(strs[1], strs[2]) ) {
					this.operator = true;
					println(new RPL_YOUREOPER(this).originString());
				} else {
					this.operator = false; // if you are operator and tries to login again, but fails, you are a tard
					println(new ERR_PASSWDMISMATCH(this).originString());
				}
			} else {
				syntaxError(input);
			}
			break;
					
		case "REHASH" :
			if(this.isOperator()) {
				println(new RPL_REHASHING(this, AnonIRC.configReader.configFile).originString());
				boolean success = AnonIRC.configReader.rehash();
				this.privmsg(anonirc.identities.AnonIRCd.getSingleton(), "Re-reading of config file was " + (success ? "successful" : "a failure"));
			} else {
				println(new ERR_NOPRIVILEGES(this).originString());
			}
			break;
		
		case "MOTD" :
			motd();
			break;
			
		/*
		 * Unimplemented commands, that may stay unimplemented forever
		 */
		case "AWAY" :
		case "WHO" :
		case "WHOWAS" :
		case "LUSERS" :
		case "VERSION" :
		case "STATS" :
		case "LINKS" :			// maybe for opers
		case "TIME" :
		case "CONNECT" :		// maybe for opers
		case "TRACE" :			// maybe for opers
		case "ADMIN" :			// maybe
		case "INFO" :
		case "SERVLIST" :
		case "SQUERY" :
		case "RESTART" :		// YES
			println("000 " + this.name() + " " + AUtil.firstWord(input) + " :Command not supported.");
			break;
		}
	}

	private void syntaxError(String input) {
		println("ERROR :Syntax error -> " + input);
	}

	@Override
	public int compareTo(IRCIdentity o) {
		return this.name.compareTo(o.name());
	}
	
	/**
	 * Disconnects the client and gracefully kills the thread. If a reason is specified (not null), an ERROR message
	 * containing <tt>reason</tt> will be sent to the client.
	 */
	public void kill(String reason) {
		if(reason != null)
			println("ERROR :" + reason);
		this.alive = false;
		try {
			this.socket.close();
		} catch (IOException e) {
			// socket has already been closed, and that is OK
		}
	}
	
	@Override
	public void privmsg(IRCIdentity from, String message) {
		println(":" + from.name() + "!" + from.name() + "@" + AnonIRC.hostcloak + " PRIVMSG " + this.name + " :" + message);
	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
		println(":" + from.name() + "!" + from.name() + "@" + AnonIRC.hostcloak + " PRIVMSG " + channel.name() + " :" + message);
	}
	
	@Override
	public void notice(IRCIdentity from, String message) {
		println(":" + from.name() + "!" + from.name() + "@" + AnonIRC.hostcloak + " NOTICE " + this.name + " :" + message);
	}

	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {
		println(":" + from.name() + "!" + from.name() + "@" + AnonIRC.hostcloak + " NOTICE " + channel.name() + " :" + message);		
	}

	/**
	 * Sends a PING message to the client with a randomized timestamp. Randomized, because otherwise it would maybe 
	 * reveal your time zone, which is bad for anonymity.
	 */
	public void ping() {
		println("PING " + random.nextInt(Integer.MAX_VALUE));
		this.waitingForPong = true;
	}

	@Override
	public String getModeString(IRCIdentity caller) {
		if( caller == this || caller.isOperator() ) {
			return ( invisible ? "i" : "" ) + ( wallops ? "w" : "" ) + ( operator ? "o" : "" );
		}
		else return null;
	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> modeChange) {
		Iterator<String> it = modeChange.iterator();
		boolean invisible = this.invisible;
		boolean wallops = this.wallops;
		boolean operator = this.operator;
		
		if( !(caller == this || caller.isOperator()) )
			return new ERR_USERSDONTMATCH(caller);
		if( modeChange.size() > 15 )
			return new AIRC_ERR_GENERIC(caller, "Too many arguments to MODE");
		
		while(it.hasNext()) {
			char[] mc = it.next().toCharArray();
			boolean mod = true;
			for(int i=0; i!=mc.length; i++) {
				switch (mc[i]) {
				case '+' :
					mod = true;
					break;
				case '-' :
					mod = false;
					break;
				case 'i' :
					invisible = mod;
					break;
				case 'w' :
					wallops = mod;
					break;
				case 'o' :
					if(caller.isOperator()) {
						operator = mod;
					} else {
						return new ERR_NOPRIVILEGES(this);
					}
					break;
				default :
					return new ERR_UMODEUNKNOWNFLAG(caller);
				}
			}
		}
		this.invisible = invisible;
		this.wallops = wallops;
		this.operator = operator;
		// TODO: also send RPL_UMODEIS & MODE to the user affected, if this was performed by an oper
		return new RPL_UMODEIS(caller, this.getModeString(caller));
	}

	@Override
	public boolean isOperator() {
		return this.operator;
	}
	
}
