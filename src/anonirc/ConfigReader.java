package anonirc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

import anonirc.util.AUtil;


public class ConfigReader {

	public String configFile = "/etc/anonirc/anonirc.conf";
	public String hostcloak = "cloak";
	public String serverName = "AnonIRC";
	public String logfile = null;
	public String motdfile = null;
	private BufferedReader br;
	private TreeMap<String, String> opersAndPasswords = new TreeMap<>();
	
	public static ConfigReader lastConfigRead = null;
	
	public ConfigReader(String filename) {
		lastConfigRead = this;
		configFile = filename;
		rehash();
	}
	
	public ConfigReader() {
		lastConfigRead = this;
		rehash();
	}
		
	/**
	 * Reread config file, and update everything in the server
	 * @return
	 */
	public synchronized boolean rehash() {
		File f = new File(configFile);
		if(!(f.exists() && f.isFile() && f.canRead())) {
			System.err.println("Could not read config file!");
			return false;
		}
		
		try {
			br = new BufferedReader(new FileReader(configFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		
		boolean success = true;
		while(true){
			int row = 0;
			String line = null;
			try {
				line = br.readLine();
				row++;
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(line == null)
				break;
			
			String[] strs = line.split(" +");
			switch(AUtil.firstWord(line)) {
			case "oper" :
				if(strs.length == 3) {
					opersAndPasswords.put(strs[1], strs[2]);
				} else {
					syntaxError(row, line + " (syntax is \"oper <nickname> <password>\")");
					success = false;
				}
				break;

			case "servername" :
				if( strs.length == 2 ) {
					serverName = strs[1];
				} else {
					syntaxError(row, line + " (syntax is \"servername <servername>\")");
					success = false;
				}
				break;
				
			case "hostcloak" :
				if( strs.length == 2 ) {
					hostcloak = strs[1];
				} else {
					syntaxError(row, line + " (syntax is \"hostcloak <hostcloak>\")");
					success = false;
				}
				break;
				
			case "log" :
			case "logfile" :
				if( strs.length == 2 ) {
					logfile = strs[1];
				} else {
					syntaxError(row, line + " (syntax is \"log <path/filename>\")");
					success = false;
				}
				break;
			
			case "motd" :
			case "motdfile" :
				if( strs.length == 2 ) {
					motdfile = strs[1];
				} else {
					syntaxError(row, line + " (syntax is \"motd <path/filename>\")");
					success = false;
				}
				break;
				
			default:
				syntaxError(row, line + " (unknown command)");
				success = false;
			}
		}
		// update stuff
		lastConfigRead = this;
		
		AnonIRC.serverName = serverName;
		AnonIRC.hostcloak = hostcloak;
		AnonIRC.logfile = logfile;
		AnonIRC.motdfile = motdfile;
		
		AnonIRC.motd = new MOTD(motdfile);
		
		return Logger.getSingleton().rehash() && success;
	}
	
	private void syntaxError(int row, String line) {
		String errorString = "Could not read config file " + this.configFile + ", error at row " + row + ": " + line;
		System.err.println(errorString);
		Logger.getSingleton().log(errorString);
	}
	
	/**
	 * Returns true if the password is correct, for any specific nickname. If so true, the user shall be considered OPER.
	 * @param nickname
	 * @param password
	 * @return
	 */
	public boolean operLogin(String nickname, String password) {
		if(opersAndPasswords == null) {
			return false;
		} else {
			String realPass = opersAndPasswords.get(nickname);
			if(realPass == null)
				return false;
			else if( realPass.equals(password) )
				return true;
			else 
				return false;
		}
	}
	
}
