package anonirc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

import anonirc.messages.*;



/**
 * A channel is something you can use to talk with multiple people at once. A "chat room."
 * @author kaos
 *
 */
public class Channel implements IRCIdentity {

	/**
	 * Used for keeping track of what channels each user is in, all access to this global variable MUST be synchronized
	 */
	private static HashMap<IRCIdentity, TreeSet<Channel>> idInChannel = new HashMap<>();
	private static TreeSet<Channel> allChannels = new TreeSet<>();
	
	protected String name;
	protected String topic = "";
	protected String whoWroteTopic = anonirc.identities.Anonymous.getSingleton().name();
	protected TreeSet<IRCIdentity> clients = new TreeSet<>();
	protected TreeSet<IRCIdentity> chanops = new TreeSet<>();
	protected TreeSet<IRCIdentity> voice = new TreeSet<>();
	protected TreeSet<String> invite = new TreeSet<>();
	protected TreeSet<IRCIdentity> banned = new TreeSet<>(); // TODO remove when client disconnects?

	/** mode i, if true: users need to be invited to join the channel. */
	protected boolean inviteOnly = false;
	/** mode k, if true: users need to supply a key (<b>channelKey</b>) to join the channel. */
	protected boolean keySet = false;
	/** mode l, if true: only at most <b>userLimit</b> users may join the channel. */
	protected boolean limitSet = false;
	/** mode L, when channel is full, or channel is invite only and user is not on the invite list, then users are redirected to <b>linkChannel</b> channel instead */
	protected boolean linkSet = false;
	/** mode m, if true: only voiced users and chanops are allowed to speak */
	protected boolean moderated = false;
	/** mode n, if true: users need to be in the channel to speak to it */
	protected boolean no_external_msgs = true;
	/** mode P, if true: channel persists even if no users are in it. Usually channels are removed when the last user leaves. Can only be set by opers. */
	protected boolean persist = false;
	/** mode q, if true: no join/part/quit messages are shown */
	protected boolean quiet = false;
	/** mode t, if true: only chanops can set topic */
	protected boolean topicLockSet = false;
	/** if mode L: this is the channel users are redirected to */
	protected String linkChannel = "void";
	/** if mode k: this is the key users need to supply when they join the channel. */
	protected String channelKey = "";
	/** if mode l: this is the number of users allowed in the channel */
	protected int userLimit = 0;
	
	/**
	 * Creates channels
	 * @param name
	 * @return
	 */
	public static Channel create(String name) {
		if(name.startsWith("#"))
			synchronized(lock) {
				return new Channel(name);
			}
		else
			return null;
	}
	
	/**
	 * Used ONLY for avoiding race fuckups in the method above
	 */
	private static Boolean lock = false;
	
	/**
	 * Returns true if the channel name is valid
	 * @param name
	 * @return
	 */
	public static boolean validChannelName(String name) {
		if(name.matches("#[^ \t\n\r\f]*"))
			return true;
		return false;
	}
	
	protected Channel(String name) {
		this.name = name;
		AnonIRC.addIRCIdentity(this);
		synchronized(allChannels) {
			Channel.allChannels.add(this);
		}
	}
	
	@Override
	public String name() {
		return this.name;
	}
	
	public String topic() {
		return this.topic;
	}
	
	@Override
	public void privmsg(IRCIdentity from, String message) {
		if(this.moderated && !voice.contains(from))
			return;
		if(this.no_external_msgs && !isInChannel(from))
			return;
		for(IRCIdentity client : clients) {
			if( client != from ) {
				client.privmsg(this, from, message);
			}
		}
	}
	
	@Override
	public void notice(IRCIdentity from, String message) {
		if(this.moderated && !voice.contains(from))
			return;
		if(this.no_external_msgs && !isInChannel(from))
			return;
		for(IRCIdentity client : clients) {
			if( client != from ) {
				client.notice(this, from, message);
			}
		}
	}
	
	@Override
	public void notice(IRCIdentity channel, IRCIdentity from, String message) {
		return;
	}

	@Override
	public void privmsg(IRCIdentity channel, IRCIdentity from, String message) {
		return;
	}
	
	/**
	 * Resolve which channel to join, if this channel is linked to another channel, that is linked to ...
	 * @param firstChannel
	 * @return
	 */
	private String resolveChannelChainLinks(String firstChannel) {
		LinkedList<String> channelChain = new LinkedList<>();
		String thisLink = firstChannel;
		channelChain.addLast(thisLink);
		
		/* The return of this method is checked for spaces in joinLinked. A space can not
		 * occur in a channel name, so we use that to indicate errors. Besides, it allows
		 * for pretty error messages. Desu desu desu desu. */
		for(int i=0; i!=15; i++) {
			IRCIdentity id = AnonIRC.findIRCIdentity(thisLink);
			if(id == null) {
				return thisLink; 
			} else if(id instanceof Channel) {
				Channel channel = (Channel)id;
				if( channel.linkSet ) {
					String nextLink = channel.linkChannel;
					if(channelChain.contains(nextLink)) return "channels was linked in a loop";
					channelChain.addLast(nextLink);
					thisLink = nextLink;
				} else {
					return thisLink;
				}
			}
		}
		return "too many channels was linked together";
	}
	
	/**
	 * Makes the id join the linked channel instead of this one, if that is allowed.
	 * @param id
	 * @return true if successful
	 */
	private Message joinLinked(IRCIdentity id, String key) {
		String channelToJoin = resolveChannelChainLinks(this.linkChannel);
		if(channelToJoin.matches(".* .*"))
			return new AIRC_ERR_GENERIC(id, "Could not join " + this.name() + " (which is set mode +L) because " + channelToJoin);
		IRCIdentity channelId = AnonIRC.findIRCIdentity(channelToJoin);
		if(channelId == null)
			channelId = Channel.create(channelToJoin);
		if(channelId instanceof Channel) {
			Channel channel = (Channel)channelId;
			if(channel.isInChannel(id)) {
				id.privmsg(channelId, anonirc.identities.AnonIRCd.getSingleton(), "Your join to " + this.name + " was redirected back here, but you were already in this channel");
				return new AIRC_OK(id);
			}
			Message m = channel.join(id, key);
			if( !m.isError() ) {
				id.privmsg(channelId, anonirc.identities.AnonIRCd.getSingleton(), "Your join to " + this.name + " was redirected to " + channelToJoin);
				return m;
			} 
 		} else {
 			return new AIRC_ERR_GENERIC(id, "Could not join channel (+L). Could not create last channel link: " + channelToJoin);
 		}
		return new AIRC_ERR_GENERIC(id, "Could not join channel (+L).");
	}
	
	/**
	 * Makes the IRCClient join the channel
	 * @param id
	 * @param key the password used to enter the channel (ignored if "mode k" is not set / keySet = false)
	 * @return
	 */
	public synchronized Message join(IRCIdentity id, String key) {
		// check if user is allowed to join
		if(limitSet && clients.size() >= userLimit && !id.isOperator()) {
			if(linkSet) 
				return joinLinked(id, key);
			return new ERR_CHANNELISFULL(id, this);
		}
		if(inviteOnly) {
			if(!invite.contains(id.name())) {
				if(linkSet)
					return joinLinked(id, key);
				return new ERR_INVITEONLYCHAN(id, this);
			} else {
				invite.remove(id.name());
			}
		}
		if(keySet && !key.equals(this.channelKey))
			return new ERR_BADCHANNELKEY(id, this);
		if(banned.contains(id))
			return new ERR_BANNEDFROMCHAN(id, this);
		// user is allowed to join channel
		synchronized(idInChannel) {
			TreeSet<Channel> chnls = idInChannel.get(id);
			if(chnls == null) {
				chnls = new TreeSet<Channel>();
				idInChannel.put(id, chnls);
			}
			chnls.add(this);
		}
		if(clients.contains(id))
			return new AIRC_OK(id);
		if(clients.isEmpty())
			chanops.add(id);
		clients.add(id);
		if(!quiet) {
			// TODO: replace with message passing system of Messages
			for(IRCIdentity c : clients) {
				if(c instanceof IRCClient)
					if(!((IRCClient)c).equals(id))
						((IRCClient)c).println(":" + id.name() + "!" + id.name() + "@" + AnonIRC.hostcloak + " JOIN " + this.name);
			}
		}
		return new JOIN(id, this);
	}

	/**
	 * Parts (=removes) the identity from the channel 
	 * @param id
	 */
	public synchronized Message part(IRCIdentity id) {
		if(!clients.contains(id))
			return new AIRC_OK(id);
		if(!quiet)
			for(IRCIdentity c : clients) // TODO: Replace with message-passing
				if(c instanceof IRCClient)
					if(!((IRCClient)c).equals(id))
						((IRCClient)c).println(":" + id.name() + "!" + id.name() + "@" + AnonIRC.hostcloak + " PART " + this.name + " :" + id.name());
		clients.remove(id);
		chanops.remove(id);
		voice.remove(id);
		synchronized(idInChannel){
			idInChannel.get(id).remove(this);
		}
		if(clients.size() == 0 && !persist) {
			// no one left in the channel, so remove it
			// do not remove channels with the persist flag (P) set
			AnonIRC.removeIRCIdentity(this);
			synchronized(allChannels) {
				Channel.allChannels.remove(this);
			}
		}
		return new AIRC_OK(id);
	}
	
	/**
	 * Returns all the clients that is in the channel
	 * @return a medium-shallow copy
	 */
	@SuppressWarnings("unchecked")
	public synchronized TreeSet<IRCIdentity> cloneTreeOfIDsInChannel() {
		return (TreeSet<IRCIdentity>)this.clients.clone();
	}
	
	/**
	 * Sets the topic of the channel
	 * @param caller
	 * @param newTopic
	 */
	public Message setTopic(IRCIdentity caller, String newTopic) {
		if(topicLockSet && !isOp(caller) && !caller.isOperator() )
			return new ERR_CHANOPRIVSNEEDED(caller, this);
		this.topic = newTopic;
		this.whoWroteTopic = caller.name();
		for(IRCIdentity c : clients) { // TODO: replace with Message-passing
			if(c instanceof IRCClient) { 
				((IRCClient)c).println(":" + caller.name() + "!" + caller.name() + AnonIRC.hostcloak + " TOPIC " + this.name + " :" + newTopic);
				getTopic((IRCClient)c);
			}
		}
		return new AIRC_OK(caller);
	}
	
	/**
	 * Returns topic-message to the caller
	 * @param client
	 * @return
	 */
	public Message getTopic(IRCClient client) {
		return new RPL_TOPIC(client, this);
	}
	
	/**
	 * Returns the topic of the channel
	 * @return
	 */
	public String getTopic() {
		return this.topic;
	}
	
	/**
	 * Derp
	 * @return
	 */
	public int getNumberOfUsers() {
		return clients.size();
	}
	
	/**
	 * Returns true if the specified user is in the channel
	 * @param who
	 * @return
	 */
	public synchronized boolean isInChannel(IRCIdentity who) {
		if(clients.contains(who))
			return true;
		else return false;
	}
	
	/**
	 * Returns true if the specified user is channel operator
	 * @param who
	 * @return
	 */
	public synchronized boolean isOp(IRCIdentity who) {
		if(chanops.contains(who)) 
			return true;
		return false;
	}
	
	/**
	 * Returns true if the specified user have voice.
	 * @param who
	 * @return
	 */
	public synchronized boolean isVoice(IRCIdentity who) {
		if(voice.contains(who)) {
			return true;
		} return false;
	}
	
	@Override
	public int compareTo(IRCIdentity o) {
		return this.name.compareTo(o.name());
	}

	/**
	 * Forcefully evicts a user from a channel
	 * @param caller
	 * @param whoToKick
	 * @param reason
	 */
	public synchronized void kick(IRCIdentity caller, IRCIdentity whoToKick, String reason) {
		if(!isOp(caller)) {
			if(caller instanceof IRCClient)
				((IRCClient)caller).println(":" + AnonIRC.serverName + " 482 " + caller.name() + " " + this.name + " :You're not channel operator"); // ERR_CHANOPRIVSNEEDED
			return;
		}
		for(IRCIdentity c : clients) {
			if(c instanceof IRCIdentity)
				((IRCClient)c).println(":" + caller.name() + "!" + caller.name() + "@" + AnonIRC.hostcloak + " KICK " + this.name + " " + whoToKick.name() + " :" + reason);
		}
		this.clients.remove(whoToKick);
		this.chanops.remove(whoToKick);
		this.banned.add(whoToKick); // it's a hard world
	}

	@Override
	public String getModeString(IRCIdentity caller) {
		if(!(isInChannel(caller) || caller.isOperator()))
			return "?";
		return getModeString();
	}
	
	/** 
	 * Always returns the string, only used by the channel itself (it always succeeds).
	 * @return
	 */
	protected String getModeString() {
		return (inviteOnly ? "i" : "") + (keySet ? "k" : "") + (limitSet ? "l" : "") + (linkSet ? "L" : "") + (moderated ? "m" : "") +
				(no_external_msgs ? "n" : "") + (persist ? "p" : "") + (quiet ? "q" : "") + (topicLockSet ? "t" : "");
	}

	@Override
	public Message setMode(IRCIdentity caller, LinkedList<String> arguments) {

		/*
		 * These variables will be updated all at once, atomically, or not at all.
		 * With the exception of chanops and voice
		 */
		boolean inviteOnly = this.inviteOnly;
		boolean keySet = this.keySet;
		boolean limitSet = this.limitSet;
		boolean linkSet = this.linkSet;
		boolean moderated = this.moderated;
		boolean no_external_msgs = this.no_external_msgs;
		boolean persist = this.persist;
		boolean quiet = this.quiet;
		boolean topicLockSet = this.topicLockSet;
		String linkChannel = new String(this.linkChannel);
		String channelKey = new String(this.channelKey);
		int userLimit = this.userLimit;
		
		if(!(this.isOp(caller) || caller.isOperator()))
			return new ERR_CHANOPRIVSNEEDED(caller, this);
		if(arguments.size() > 15)
			return new AIRC_ERR_GENERIC(caller, "Too many arguments for MODE");
		Iterator<String> it = arguments.iterator();
		while(it.hasNext()){
			String argument = it.next();
			
			boolean mod = true;
			char[] c_arg = argument.toCharArray();
			for(int c=0; c!=c_arg.length; c++) {
				switch (c_arg[c]) {
				case '+' :
					mod = true;
					break;
				case '-' :
					mod = false;
					break;
				case 'i' :
					inviteOnly = mod;
					break;
				case 'n' :
					no_external_msgs = mod;
					break;
				case 'm' :
					moderated = mod;
					break;
				case 'P' :
					if(caller.isOperator()) {
						persist = mod;
						break;
					} else {
						return new AIRC_SECURITY_VIOLATION(caller, "Only opers are allowed to set mode +P", this);
					}
				case 'q' :
					quiet = mod;
					break;
				case 't' :
					topicLockSet = mod;
					break;
				case 'b' :
					if(it.hasNext()) {
						String whomToBan = it.next();
						IRCIdentity id = AnonIRC.findIRCIdentity(whomToBan);
						if(id != null) {
							if(mod) this.kick(caller, id, "");
							else synchronized(this) { this.banned.remove(id); }
						} else {
							return new ERR_NOSUCHNICK(caller, whomToBan);
						}
					} else {
						return new AIRC_ERR_GENERIC(caller, "You need to supply a user to ban. Example: MODE #channel +b nickname");
					}
					break;
				case 'L' :
					if(!caller.isOperator())
						return new ERR_NOPRIVILEGES(this);
					if(mod) {
						linkSet = true;
						if(it.hasNext()) {
							linkChannel = it.next();
							break;
						} else {
							return new AIRC_ERR_GENERIC(caller, "You need to supply a channel to link to. Example: MODE #channel +L #otherchannel");
						}
					} else {
						linkSet = false;
						linkChannel = null;
					}
					break;
				case 'l' :
					if(mod) {
						limitSet = true;
						if(it.hasNext()) {
							String userLimitStr = it.next();
							try {
								userLimit = Integer.parseInt(userLimitStr);
							} catch (NumberFormatException e) {
								return new AIRC_ERR_GENERIC(caller, userLimitStr + " is not a number.");
							}
						} else {
							return new AIRC_ERR_GENERIC(caller, "You need to supply the maximum number of users. Example: MODE #channel +l 10");
						}
					} else {
						limitSet = false;
						userLimit = -1;
					}
					break;
				case 'k' :
					if(mod) {
						keySet = true;
						if(it.hasNext()) {
							channelKey = it.next();
						} else {
							return new AIRC_ERR_GENERIC(caller, "You need to supply the channel key. Example: MODE #channel +k th3p@szw0rd");
						}
					} else {
						keySet = false;
						channelKey = "";
					}
					break;
				case 'o' :
					if(it.hasNext()) {
						String nickname = it.next();
						IRCIdentity id = AnonIRC.findIRCIdentity(nickname);
						if(id != null && this.isInChannel(id)) {
							synchronized(this) {
								if(mod) this.chanops.add(id);
								else this.chanops.remove(id);
							}
						} else {
							return new ERR_USERNOTINCHANNEL(caller, nickname, this);
						}
					} else {
						return new AIRC_ERR_GENERIC(caller, "You need to supply whom to op. Example: MODE #channel +o niceDude");
					}
					break;
				case 'v' :
					if(it.hasNext()) {
						String nickname = it.next();
						IRCIdentity id = AnonIRC.findIRCIdentity(nickname);
						if(id != null && this.isInChannel(id)) {
							synchronized(this) {
								if(mod) this.voice.add(id);
								else this.voice.remove(id);
							}
						} else {
							return new ERR_USERNOTINCHANNEL(caller, nickname, this);
						}
					} else {
						return new AIRC_ERR_GENERIC(caller, "You need to supply whom to voice. Example: MODE #channel +v niceDude");
					}
					break;
				default :
					return new ERR_UNKNOWNMODE(caller, this, "" + c_arg[c]);
				}
			}
		}
		synchronized(this) {
			this.inviteOnly = inviteOnly;
			this.keySet = keySet;
			this.limitSet = limitSet;
			this.linkSet = linkSet;
			this.moderated = moderated;
			this.no_external_msgs = no_external_msgs;
			this.persist = persist;
			this.quiet = quiet;
			this.topicLockSet = topicLockSet;
			this.linkChannel = linkChannel;
			this.channelKey = channelKey;
			this.userLimit = userLimit;
		}
		String suppliedModeString = "";
		for(String s : arguments)
			suppliedModeString += s + " ";
		String rpl_channelmodeis = ":" + caller.name() + "!" + caller.name() + "@" + AnonIRC.serverName + " MODE " + this.name + " " + suppliedModeString.trim();
		for(IRCIdentity id : clients) { 
			if(id instanceof IRCClient) {
				((IRCClient)id).println(rpl_channelmodeis);
			}
		}
		return new AIRC_OK(caller);
	}

	@Override
	public boolean isOperator() {
		return false;
	}

	/**
	 * Invites a user to this channel
	 * @param caller who is inviting
	 * @param nickname the name of whoever is being invited
	 */
	public Message invite(IRCClient caller, String nickname) {
		if(inviteOnly && !isOp(caller) && !caller.isOperator()) {
			return new ERR_CHANOPRIVSNEEDED(caller, this);
		}
		IRCIdentity id = AnonIRC.findIRCIdentity(nickname);
		if(id == null)
			return new ERR_NOSUCHCHANNEL(caller, nickname);
		synchronized(this) {
			invite.add(nickname);	
		}
		// TODO: Replace this with message passing once that is implemented
		if(caller instanceof IRCClient)
			((IRCClient)caller).println(":" + caller.name() + "!" + caller.name() + "@" + AnonIRC.serverName + " INVITE " + nickname + " " + this.name );
		if(id instanceof IRCClient)
			((IRCClient)id).println(":" + caller.name() + "!" + caller.name() + "@" + AnonIRC.serverName + " INVITE " + nickname + " " + this.name );
		if(!quiet) this.privmsg(anonirc.identities.AnonIRCd.getSingleton(), caller.name() + " invited " + nickname);
		return new AIRC_OK(caller);
	}
	
	/**
	 * removes the IRCClient from all data structures related to the Channel class.
	 * @param id
	 */
	public static void forgetEverythingAboutThisClient(IRCIdentity id) {
		TreeSet<Channel> chnls;
		synchronized(idInChannel) {
			chnls = allChannelsOfUser(id);
		}
		for(Channel chnl : chnls) {
			chnl.part(id);
			synchronized(chnl) {
				chnl.banned.remove(id);
			}
		}
		synchronized(idInChannel) {
			idInChannel.remove(id);
		}
	}
	
	/**
	 * Returns a (shallow copy) tree with all channels where a user is present
	 * @param id
	 * @return a medium-shallow copy
	 */
	@SuppressWarnings("unchecked")
	public static TreeSet<Channel> allChannelsOfUser(IRCIdentity id) {
		synchronized(idInChannel) {
			TreeSet<Channel> channels = (TreeSet<Channel>) idInChannel.get(id);
			if(channels != null) {
				return (TreeSet<Channel>)channels.clone();
			} else {
				return new TreeSet<>();
			}
		}
	}

	/**
	 * Notifies everyone that is in the same channel of the specified user, that it has changed its nickname. It also
	 * sends the same message to the user that changed its own nickname, <i>even if that user is not in any channel.</i>
	 * @param ircClient
	 * @param newName
	 */
	public static void updateNicknameInAllChannels(IRCClient ircClient, String oldName, String newName) {
		String nickUpdateStr = ":" + oldName + "!" + oldName + "@" + AnonIRC.hostcloak + " NICK " + newName;
		TreeSet<Channel> chnls;
		synchronized(idInChannel) {
			chnls = (TreeSet<Channel>) idInChannel.get(ircClient);
			if(chnls == null) 
				chnls = new TreeSet<Channel>();
		}
		TreeSet<IRCClient> clientsWrittenTo = new TreeSet<>();
		ircClient.println(nickUpdateStr);
		clientsWrittenTo.add(ircClient);
		for(Channel c : chnls) {
			synchronized(c) {
				// TODO: Replace this with message-passing, once that is implemented
				for(IRCIdentity id : c.clients) {
					if(id instanceof IRCClient) {
						if(clientsWrittenTo.contains(id))
							continue;
						((IRCClient) id).println(nickUpdateStr);
						clientsWrittenTo.add((IRCClient)id);
					}
				}
			}
		}
	}
	
	/**
	 * Returns an iterable of all channels that are known
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Iterable<Channel> allChannels() {
		return (Iterable<Channel>) allChannels.clone();
	}
	
	/**
	 * Returns the number of channels that are known
	 * @return
	 */
	public static int numberOfChannels() {
		return allChannels.size();
	}
	
}
