package anonirc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class AnonIRC {
	
	public static String serverName = "AnonIRC";
	public static String hostcloak = "cloak";
	public static String logfile = null;
	public static String motdfile = null;
	public static MOTD motd;
	public static ConfigReader configReader;
	private static HashMap<String, IRCIdentity> stringToID = new HashMap<>();
	private static long systemUniqueNumber = 0;
	
	/**
	 * Creates a number that is guaranteed to be unique for this instance of the AnonIRC server
	 * @return
	 */
	public synchronized static long getUniqueID() {
		return systemUniqueNumber++;
	}
	
	/**
	 * Adds the IRCIdentity to the list of active & known IRCIdentities.
	 * @param id
	 * @return true if successful (no one else had that name)
	 */
	public synchronized static boolean addIRCIdentity(IRCIdentity id) {
		if(stringToID.containsKey(id.name())) {
			return false;
		} else {
			stringToID.put(id.name(), id);
			return true;
		}
	}
	
	public synchronized static void removeIRCIdentity(IRCIdentity id) {
		stringToID.remove(id.name());
	}
	
	public synchronized static IRCIdentity findIRCIdentity(String name) {
		return stringToID.get(name);
	}
	
	public synchronized static void updateNameOfIdentity(String oldName, IRCIdentity id) {
		stringToID.remove(oldName);
		stringToID.put(id.name(), id);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Iterator<String> it = Arrays.asList(args).iterator();
		String arg;
		String logfile = null;
		String configfile = null;
		while(it.hasNext()) {
			arg = it.next();
			switch(arg) {
			case "--log" :
			case "--logfile" :
				if(it.hasNext())
					logfile = it.next();
				else {
					System.out.println("--log <filename>");
					System.exit(-1);
				}
				break;
			case "--conf" :
			case "--config" :
				if(it.hasNext())
					configfile = it.next();
				else {
					System.out.println("--config <filename>");
					System.exit(-1);
				}
				break;
			}
				
		} 
		if(configfile != null)
			configReader = new ConfigReader(configfile);
		if(logfile != null)
			AnonIRC.logfile = logfile;
		if(motdfile != null)
			motd = new MOTD(motdfile);
		else
			motd = new MOTD();
		
		new ClientListener(6667);
		Logger.getSingleton().log("Started.");
	}

}
