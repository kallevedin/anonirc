package anonirc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class MOTD {

	public LinkedList<String> motdLines = new LinkedList<>(); 
	private String motdFile;
	
	/**
	 * Create MOTD-object with boring default MOTD message
	 */
	public MOTD() {
		motdLines = defaultMotd();
	}
	
	public MOTD(String filename) {
		motdFile = filename;
		if( !readMotdFile() )
			motdLines = defaultMotd();
	}
	
	private LinkedList<String> defaultMotd() {
		LinkedList<String> r = new LinkedList<>();
		r.addLast("~~ AnonIRC ~~");
		return r;
	}
	
	private boolean readMotdFile() {
		File f = new File(motdFile);
		BufferedReader br = null;
		String line = null;
		if(!(f.exists() && f.isFile() && f.canRead())) {
			System.err.println("can not read MOTD file: " + motdFile);
			return false;
		}
		try {
			br = new BufferedReader(new FileReader(motdFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		while(true) {
			try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			if(line == null) break;
			motdLines.addLast(line);
		}
		return true;
	}

}
